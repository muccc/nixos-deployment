{ self, formats }:
(formats.yaml { }).generate "sops.yaml" {
  creation_rules =
    [
      {
        path_regex = "secrets/credentials\\.yaml$";
        key_groups = [ { age = self.lib.humanoids.getAgeForGroup "infra-admins"; } ];
      }
      {
        path_regex = "secrets/hetzner\\.yaml$";
        key_groups = [ { age = self.lib.humanoids.getAgeForGroup "hetzner"; } ];
      }
    ]
    ++ self.lib.hosts.mapAllHostsToList (
      host: values: {
        path_regex = "secrets/${host}(/[^/]+)?\\.yaml$";
        key_groups = [ { age = [ values.age ] ++ self.lib.humanoids.getAgeForGroup "infra-admins"; } ];
      }
    )
    ++ self.lib.hosts.hostsForSopsRules (
      regex: hosts: {
        path_regex = "secrets/${regex}\\.yaml$";
        key_groups = [
          { age = (builtins.map (host: host.age) hosts) ++ self.lib.humanoids.getAgeForGroup "infra-admins"; }
        ];
      }
    );
}
