{
  gobject-introspection,
  python3Packages,
  stdenv,
  writers,
  wrapGAppsNoGuiHook,
}:
let
  name = "a2dp-agent";
in
stdenv.mkDerivation {
  inherit name;

  nativeBuildInputs = [
    gobject-introspection
    wrapGAppsNoGuiHook
  ];

  src = writers.writePython3 "a2dp-agent" {
    libraries = with python3Packages; [
      dbus-python
      pygobject3
    ];
  } (builtins.readFile ./../static/multimedia/a2dp-agent.py);

  dontUnpack = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin
    cp -r $src $out/bin/${name}

    runHook postInstall
  '';

  meta.mainProgram = name;
}
