{
  system.stateVersion = "24.11";

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
    };
  };

  networking = {
    hostName = "nginx";
    domain = "club.muc.ccc.de";
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "5e:e0:1e:54:91:1f";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  muccc.acme-dns.certs = {
    "nginx.club.muc.ccc.de" = "acme-dns-nginx";
    "hass.club.muc.ccc.de" = "acme-dns-hass";
    "pve.club.muc.ccc.de" = "acme-dns-pve";
    "cache.club.muc.ccc.de" = "acme-dns-cache";
  };

  services.nginx = {
    enable = true;

    virtualHosts."nginx.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      default = true;
      locations."/".return = "200 'no content left on device'";
    };

    virtualHosts."pve.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations = {
        "/" = {
          proxyPass = "https://gustl.mgmt.club.muc.ccc.de:8006";
          proxyWebsockets = true;
        };
      };
    };

    virtualHosts."hass.club.muc.ccc.de" = {
      forceSSL = true;
      enableACME = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://homeassistant.club.muc.ccc.de:8123";
        proxyWebsockets = true;
      };
    };

    virtualHosts."nixos-passthru-cache" = {
      acmeRoot = null;
    };
  };

  muccc.nix-cache = {
    enable = true;
  };
}
