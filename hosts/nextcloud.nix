{
  config,
  pkgs,
  ...
}:
{
  system.stateVersion = "23.05";

  sops = {
    secrets.nextcloud_admin_pass = {
      owner = "nextcloud";
    };

    secrets.nextcloud_exporter_pass = {
      owner = "nextcloud-exporter";
    };
  };

  fileSystems = {
    "/var/lib/nextcloud" = {
      device = "/dev/disk/by-label/nextcloud-data";
    };
  };

  networking = {
    hostName = "nextcloud";
    domain = "club.muc.ccc.de";

    firewall.allowedTCPPorts = [ config.services.prometheus.exporters.redis.port ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "5E:48:49:E4:B1:B1";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud30;

    hostName = "cloud.muc.ccc.de";
    https = true;

    database.createLocally = true;
    configureRedis = true;

    config = {
      dbtype = "pgsql";
      adminpassFile = config.sops.secrets.nextcloud_admin_pass.path;
    };

    settings = {
      hide_login_form = false;
      user_oidc = {
        allow_multiple_user_backends = 0;
      };
      default_phone_region = "DE";
      trusted_proxies = [
        "::1"
        "127.0.0.1"
      ];
      maintenance_window_start = 4;
      updatechecker = false;
      no_unsupported_browser_warning = true;
      activity_expire_days = 60;
      versions_retention_obligation = "auto, 3";
      trashbin_retention_obligation = "auto, 7";
    };

    phpOptions = {
      "opcache.interned_strings_buffer" = "32";
      "opcache.max_accelerated_files" = "100000";
      "opcache.memory_consumption" = "256";
      "opcache.jit" = "1255";
      "opcache.jit_buffer_size" = "256M";
    };

    extraApps = {
      inherit (config.services.nextcloud.package.packages.apps)
        calendar
        polls
        groupfolders
        forms
        user_oidc
        richdocuments
        ;

      calendar_resource_management = pkgs.fetchNextcloudApp {
        url = "https://github.com/nextcloud-releases/calendar_resource_management/releases/download/v0.8.0/calendar_resource_management-v0.8.0.tar.gz";
        sha256 = "sha256-ILPzG3bNnohyuXsCneR+Z2OheyLFqLKX74lI+7lkMKA=";
        license = "agpl3Plus";
      };
    };

    extraAppsEnable = true;
  };

  muccc.acme-dns.certs = {
    "nextcloud.club.muc.ccc.de" = "acme-dns-nextcloud";
    "cloud.muc.ccc.de" = "acme-dns-nextcloud";
    "cloud-office.muc.ccc.de" = "acme-dns-nextcloud";
  };

  services.nginx.virtualHosts = {
    "nextcloud.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/".return = "301 https://cloud.muc.ccc.de";
    };
    "cloud.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
    };
    "cloud-office.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations = {
        # static files
        "^~ /browser" = {
          proxyPass = "http://localhost:9980";
        };
        # WOPI discovery URL
        "^~ /hosting/discovery" = {
          proxyPass = "http://localhost:9980";
        };

        # Capabilities
        "^~ /hosting/capabilities" = {
          proxyPass = "http://localhost:9980";
        };

        # download, presentation, image upload and websocket
        "~ ^/cool" = {
          proxyPass = "http://localhost:9980";
          proxyWebsockets = true;
        };

        # Admin Console websocket
        "^~ /cool/adminws" = {
          proxyPass = "http://localhost:9980";
          proxyWebsockets = true;
          extraConfig = ''
            proxy_read_timeout 36000s;
          '';
        };
      };
    };
  };

  virtualisation.oci-containers = {
    backend = "docker";
    containers.collabora = {
      image = "collabora/code";
      imageFile = pkgs.dockerTools.pullImage {
        imageName = "collabora/code";
        imageDigest = "sha256:f6d2da45ef1b8db44b0d6a11fe8c0bb98022c0f2befb859b8565962ec58b2afd";
        sha256 = "0a2q8i7yfpzj58jc2hvma6fkmm28sjdd4ap5mwbq39dqr55alcpa";
        finalImageName = "collabora/code";
        finalImageTag = "24.04.11.3.1";
      };
      ports = [ "9980:9980" ];
      environment = {
        domain = "cloud.muc.ccc.de";
        extra_params = "--o:ssl.enable=false --o:ssl.termination=true";
      };
      extraOptions = [
        "--cap-add"
        "MKNOD"
      ];
    };
  };

  services.prometheus.exporters.redis.enable = true;
  services.prometheus.exporters.nextcloud = {
    enable = true;
    openFirewall = true;

    url = "https://cloud.muc.ccc.de";
    username = "root";
    passwordFile = config.sops.secrets.nextcloud_exporter_pass.path;
  };
}
