{
  config,
  lib,
  nodes,
  ...
}:
let
  genPrometheusTargets =
    filterfn: targetfn:
    let
      hosts = lib.filter filterfn (lib.attrValues nodes);
    in
    map targetfn hosts;

  checkAll = checks: node: lib.all (f: f node.config) checks;
  exporterCheck = exporterName: config: config.services.prometheus.exporters.${exporterName}.enable;
  infraCheck = config: lib.elem "infra" config.deployment.tags;

  nodeExporterTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "node")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.node.port}"
      )
    # manual targets
    ++ [
      "gustl.mgmt.club.muc.ccc.de:9100"
      "space.club.muc.ccc.de:9100"
      "xaver.muc.ccc.de:9100"
    ];

  postgresTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "postgres")
      ])
      (
        c:
        "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.postgres.port}"
      );

  knotTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "knot")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.knot.port}"
      );

  kresdTargets = genPrometheusTargets (c: c.config.muccc.recursor.enable) (
    c: "${c.config.deployment.targetHost}:8053"
  );

  postfixTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "postfix")
      ])
      (
        c:
        "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.postfix.port}"
      );

  nextcloudTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "nextcloud")
      ])
      (
        c:
        "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.nextcloud.port}"
      );

  redisTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "redis")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.redis.port}"
      );

  forgejoTargets = genPrometheusTargets (c: c.config.services.forgejo.enable) (
    c: "${c.config.services.forgejo.settings.server.DOMAIN}"
  );

  nginxTargets =
    genPrometheusTargets
      (checkAll [
        infraCheck
        (exporterCheck "nginx")
      ])
      (
        c: "${c.config.deployment.targetHost}:${toString c.config.services.prometheus.exporters.nginx.port}"
      )
    # manual targets
    ++ [ ];

  httpNginxVhosts =
    lib.filter (host: !(lib.elem host [ "localhost" ])) (
      lib.flatten (
        map (
          node:
          lib.mapAttrsToList (
            k: v: if v.serverName != null then v.serverName else k
          ) node.config.services.nginx.virtualHosts
        ) (lib.filter (node: node.config.services.nginx.enable) (lib.attrValues nodes))
      )
    )
    # manual targets
    ++ [
      "muc.ccc.de"
      # "rad1o.badge.events.ccc.de"
      "otrs.muc.ccc.de"
    ];

  /*
    snmpTargets = [
      # Switches
      "cs2960.mgmt.club.muc.ccc.de" # Core
      "ds2510g48.mgmt.club.muc.ccc.de" # Environment
      "ds2960.mgmt.club.muc.ccc.de" # PoE
      # garching / mikrotik router
      #"192.168.178.1"
    ];
  */

  # common relabel configs, append or apply in jobs as needed
  relabelHostnameFromInstance = {
    source_labels = [ "__address__" ];
    regex = "([^:]+):\\d+";
    target_label = "hostname";
  };
  commonRelabelConfigs = [ relabelHostnameFromInstance ];
in
{
  system.stateVersion = "23.05";

  sops = {
    secrets.pve_env = { };
    secrets.grafana-oidc-client = {
      owner = "grafana";
    };
    secrets.grafana-oidc-secret = {
      owner = "grafana";
    };
    secrets.acme-dns-prometheus = {
      owner = "acme";
    };
    secrets.acme-dns-grafana = {
      owner = "acme";
    };
  };

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-uuid/EFA8-248B";
      fsType = "vfat";
    };
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/a76d7dc8-01c2-411a-909c-b5713142e56c"; } ];

  networking = {
    hostName = "prometheus";
    domain = "club.muc.ccc.de";
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "5a:66:9d:22:e5:44";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  services.kresd = {
    enable = true;
    listenPlain = [
      "127.0.0.1:53"
      "[::1]:53"
    ];
  };

  muccc.snmpExporter = {
    enable = true;
    librenmsSubdirs = [ "arubaos" ];
    config = {
      auths.aruba = {
        username = "prometheus";
        password = "uY4uUB7JLpekboFTNVzty1F";
        auth_protocol = "SHA";
        priv_protocol = "AES";
        priv_password = "uY4uUB7JLpekboFTNVzty1F";
        security_level = "authPriv";
        version = 3;
      };
      modules.aruba = {
        timeout = "30s";
        walk = [
          "1.3.6.1.4.1.14823" # enterprise aruba
        ];
      };
    };
    targets.aruba = {
      module = "aruba";
      auth = "aruba";
      interval = "30s";
      targets = [ "wifi.mgmt.club.muc.ccc.de" ];
    };
  };

  services.prometheus = {
    enable = true;
    enableReload = true;
    extraFlags = [
      "--query.max-concurrency=100"
      "--web.max-connections=512"
      "--storage.tsdb.retention.time=90d"
      "--storage.tsdb.wal-compression"
    ];

    webExternalUrl = "https://prometheus.club.muc.ccc.de/";

    exporters.pve.enable = true;
    exporters.pve.environmentFile = config.sops.secrets.pve_env.path;

    exporters.blackbox.enable = true;
    exporters.blackbox.configFile = ../static/prometheus/blackbox/blackbox.yaml;

    ruleFiles = [
      # rules from github.com/samber/awesome-prometheus-alerts
      ../static/prometheus/rules/embedded-exporter.yml
      ../static/prometheus/rules/node-exporter.yml
      ../static/prometheus/rules/blackbox-exporter.yml

      ../static/prometheus/rules/muccc.yaml
    ];

    globalConfig = {
      scrape_interval = "30s";
    };

    alertmanagers = [
      {
        path_prefix = "/alertmanager";
        static_configs = [ { targets = [ "localhost:9093" ]; } ];
      }
    ];

    scrapeConfigs =
      [
        {
          job_name = "prometheus";
          static_configs = [ { targets = [ "localhost:9090" ]; } ];
        }
        {
          job_name = "alertmanager";
          metrics_path = "/alertmanager/metrics";
          static_configs = [ { targets = [ "localhost:9093" ]; } ];
        }
        {
          job_name = "node-exporter";
          static_configs = [ { targets = nodeExporterTargets; } ];
          relabel_configs = commonRelabelConfigs;
          metric_relabel_configs = [
            {
              source_labels = [
                "__name__"
                "name"
              ];
              separator = ";";
              regex = "^node_systemd_unit_state;(.*)\.(automount|device|mount|path|scope|slice|swap|target)$";
              action = "drop";
            }
          ];
        }
        {
          job_name = "proxmox";
          static_configs = [ { targets = [ "gustl.mgmt.club.muc.ccc.de" ]; } ];
          metrics_path = "/pve";
          relabel_configs = [
            {
              source_labels = [ "__address__" ];
              target_label = "__param_target";
            }
            {
              source_labels = [ "__param_target" ];
              target_label = "instance";
            }
            {
              target_label = "__address__";
              replacement = "127.0.0.1:${toString config.services.prometheus.exporters.pve.port}";
            }
          ];
        }
        {
          job_name = "knot-exporter";
          static_configs = [ { targets = knotTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "kresd";
          static_configs = [ { targets = kresdTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "mumble-exporter";
          scrape_interval = "2m";
          static_configs = [ { targets = [ "fasel.muc.ccc.de:64738" ]; } ];
          relabel_configs =
            [
              {
                source_labels = [ "__address__" ];
                target_label = "__param_host";
              }
              {
                source_labels = [ "__param_host" ];
                target_label = "instance";
              }
            ]
            ++ commonRelabelConfigs
            ++ [
              {
                target_label = "__address__";
                replacement = "127.0.0.1:8778";
              }
            ];
        }
        {
          job_name = "postfix-exporter";
          static_configs = [ { targets = postfixTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "rspamd";
          scheme = "https";
          metrics_path = "/rspamd/metrics";
          basic_auth = {
            username = "briaftraeger";
            password = "briafgeheimnis";
          };
          static_configs = [ { targets = [ "briafzentrum.muc.ccc.de" ]; } ];
        }
        {
          job_name = "api";
          scheme = "https";
          static_configs = [ { targets = [ "api.muc.ccc.de" ]; } ];
        }
        {
          job_name = "authentik";
          scheme = "http";
          static_configs = [ { targets = [ "auth.club.muc.ccc.de:9300" ]; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "blackbox";
          static_configs = [ { targets = [ "localhost:9115" ]; } ];
        }
        {
          job_name = "blackbox-http";
          metrics_path = "/probe";
          params = {
            module = [ "http_2xx" ];
          };
          static_configs = [ { targets = httpNginxVhosts; } ];
          relabel_configs = [
            {
              source_labels = [ "__address__" ];
              target_label = "__param_target";
            }
            {
              source_labels = [ "__param_target" ];
              target_label = "instance";
            }
            {
              target_label = "__address__";
              replacement = "localhost:9115";
            }
          ];
        }
        {
          job_name = "nextcloud";
          static_configs = [ { targets = nextcloudTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "postgres";
          static_configs = [ { targets = postgresTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "forgejo";
          scheme = "https";
          static_configs = [ { targets = forgejoTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "redis-exporter";
          static_configs = [ { targets = redisTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "nginx";
          static_configs = [ { targets = nginxTargets; } ];
          relabel_configs = commonRelabelConfigs;
        }
        {
          job_name = "hass";
          metrics_path = "/api/prometheus";
          scheme = "https";
          static_configs = [
            { targets = [ "hass.club.muc.ccc.de" ]; }
          ];
        }
      ]
      ++ map (
        {
          targets,
          module,
          auth,
          interval,
        }:
        {
          job_name = "snmp_${module}";
          scrape_interval = interval;
          metrics_path = "/snmp";
          params.module = [ module ];
          static_configs = [
            {
              inherit targets;
              labels.source = config.networking.hostName;
            }
          ];
          relabel_configs = [
            {
              source_labels = [ "__address__" ];
              target_label = "__param_target";
            }
            {
              source_labels = [ "__param_target" ];
              target_label = "instance";
            }
            {
              target_label = "__address__";
              replacement = "${config.networking.hostName}:${toString config.services.prometheus.exporters.snmp.port}";
            }
            {
              target_label = "__param_auth";
              replacement = auth;
            }
          ];
        }
      ) (lib.attrValues config.muccc.snmpExporter.targets);
  };

  services.prometheus-mumble-exporter.enable = true;

  services.prometheus.alertmanager = {
    enable = true;
    listenAddress = "[::1]";
    webExternalUrl = "https://prometheus.club.muc.ccc.de/alertmanager/";
    extraFlags = [
      "--cluster.listen-address=[::1]:9094"
    ];

    configuration = {
      route = {
        group_by = [
          "job"
          "instance"
          "severity"
        ];
        group_wait = "30s";
        group_interval = "5m";
        repeat_interval = "12h";
        receiver = "infra";
        routes = [
          {
            matchers = [
              "severity = critical"
              "instance = luftschleuse.club.muc.ccc.de"
            ];
            receiver = "muccc";
            repeat_interval = "1h";
          }
          {
            matchers = [ "severity = critical" ];
            receiver = "infra";
            repeat_interval = "1h";
          }
          {
            matchers = [ "severity = warning" ];
            receiver = "infra";
            repeat_interval = "6h";
          }
          {
            matchers = [ "severity = info" ];
            receiver = "devnull";
          }
        ];
      };

      inhibit_rules = [
        {
          source_matchers = [ "severity=critical" ];
          target_matchers = [ "severity=warning" ];
          equal = [
            "job"
            "instance"
            "severity"
          ];
        }
      ];

      receivers = [
        { name = "devnull"; }
        {
          name = "infra";
          webhook_configs = [
            #{
            #  send_resolved = false;
            #  url = "http://localhost:8000/infra-muccc-alerts";
            #}
            { url = "https://ntfy.bpletza.de/alertmanager/muccc"; }
          ];
        }
        {
          name = "muccc";
          webhook_configs = [
            # { url = "http://localhost:8000/muccc"; }
          ];
        }
      ];
    };
  };

  services.prometheus.alertmanagerIrcRelay = {
    enable = false;

    settings = {
      http_host = "localhost";
      http_port = 8000;

      irc_host = "irc.hackint.org";
      irc_port = 6697;
      irc_nickname = "alurm";

      irc_channels = [
        { name = "#infra-muccc-alerts"; }
        #{ name = "#muccc"; }
      ];

      msg_template = "Alert {{ .Labels.alertname }} ({{ .Labels.severity }}) on {{ .Labels.job }}/{{ .Labels.instance }} is {{ .Status }}";
    };
  };

  services.grafana = {
    enable = true;
    settings = {
      server = {
        http_addr = "::1";
        http_port = 3000;

        domain = "grafana.club.muc.ccc.de";
        root_url = "https://grafana.club.muc.ccc.de/";
      };

      "auth" = {
        "oauth_allow_insecure_email_lookup" = "true";
        "signout_redirect_url" = "https://auth.muc.ccc.de/application/o/grafana/end-session/";
      };

      "auth.generic_oauth" = {
        enabled = true;
        name = "authentik";
        client_id = "$__file{${config.sops.secrets.grafana-oidc-client.path}}";
        client_secret = "$__file{${config.sops.secrets.grafana-oidc-secret.path}}";
        auth_url = "https://auth.muc.ccc.de/application/o/authorize/";
        token_url = "https://auth.muc.ccc.de/application/o/token/";
        api_url = "https://auth.muc.ccc.de/application/o/userinfo/";
        scopes = "openid profile email";
        login_attribute_path = "preferred_username";
        allow_assign_grafana_admin = true;
        role_attribute_path = "grafana_role";
        auto_login = true;
      };
    };
  };

  muccc.acme-dns.certs = {
    "prometheus.club.muc.ccc.de" = "acme-dns-prometheus";
    "grafana.club.muc.ccc.de" = "acme-dns-grafana";
  };

  services.nginx = {
    enable = true;
    virtualHosts."prometheus.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      basicAuth.monitoring = "neigschaut";
      locations = {
        "/" = {
          proxyPass = "http://[::1]:9090/";
          extraConfig = ''
            satisfy any;
            allow 127.0.0.1;
            allow ::1;
            deny all;
          '';
        };
        "/alertmanager/" = {
          proxyPass = "http://[::1]:9093";
        };
      };
    };

    virtualHosts."grafana.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations = {
        "/" = {
          proxyPass = "http://[::1]:3000";
          proxyWebsockets = true;
        };
      };
    };
  };
}
