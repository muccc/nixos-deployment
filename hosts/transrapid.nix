{
  config,
  lib,
  ...
}:
{
  system.stateVersion = "24.11";

  sops = {
    secrets = {
      wireguard_private_key = {
        owner = "systemd-network";
      };
      wireguard_psk_stammstrecke = {
        owner = "systemd-network";
      };
      wireguard_psk_oaarchkatzl = {
        owner = "systemd-network";
      };
      acme-dns-transrapid = {
        owner = "acme";
      };
      ppp_mnet = { };
    };
  };

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  hardware.cpu.intel.updateMicrocode = true;
  hardware.enableRedistributableFirmware = true;

  muccc.qemu-guest.enable = false;

  boot.initrd.kernelModules = [
    "mpt3sas"
    "mgag200"
  ];
  boot.initrd.availableKernelModules = [
    "ehci_pci"
    "ahci"
    "usb_storage"
    "usbhid"
    "sd_mod"
  ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/2557f72e-1436-4279-915c-247df4e76fe5";
    fsType = "btrfs";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/9371-A939";
    fsType = "vfat";
    options = [
      "fmask=0022"
      "dmask=0022"
    ];
  };

  networking.domain = "muc.ccc.de";

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = lib.mkForce "1";
    "net.ipv6.conf.all.forwarding" = lib.mkForce "1";
  };

  systemd.network = {
    networks."10-bond-eno1" = {
      matchConfig.MACAddress = "d4:ae:52:bf:e5:01";
      networkConfig.Bond = "bond";
    };
    networks."10-bond-eno2" = {
      matchConfig.MACAddress = "d4:ae:52:bf:e5:02";
      networkConfig.Bond = "bond";
    };

    netdevs."10-bond" = {
      netdevConfig = {
        Name = "bond";
        Kind = "bond";
      };
      bondConfig = {
        Mode = "802.3ad";
        TransmitHashPolicy = "layer2+3";
      };
    };

    networks."10-bond" = {
      matchConfig.Name = "bond";
      linkConfig.RequiredForOnline = false;
      networkConfig = {
        LLDP = true;
        EmitLLDP = true;
        VLAN = [
          "mgmt"
          "environment"
          "iot"
          "access"
          "mnet"
        ];
      };
    };

    netdevs."10-mgmt" = {
      netdevConfig = {
        Kind = "vlan";
        Name = "mgmt";
      };
      vlanConfig.Id = 2032;
    };

    networks."10-mgmt" = {
      matchConfig.Name = "mgmt";
      networkConfig =
        let
          ip6 = "2a01:7e01:e003:8b23::1";
        in
        {
          Address = [
            "${ip6}/64"
            "10.189.23.1/24"
            "83.133.178.33/27"
          ];
          IPv6AcceptRA = false;
          DNS = [ "2a01:7e01:e003:8b00::53" ];
          Domains = [ "club.muc.ccc.de" ];
          LLDP = true;
          EmitLLDP = true;
        };
      ipv6AcceptRAConfig = {
        RouteMetric = 23;
      };
    };

    netdevs."10-iot" = {
      netdevConfig = {
        Kind = "vlan";
        Name = "iot";
      };
      vlanConfig.Id = 2430;
    };

    networks."10-iot" = {
      matchConfig.Name = "iot";
      networkConfig = {
        Address = [
          "10.189.16.1/24"
          "2a01:7e01:e003:8b16::1/64"
        ];
        IPv6AcceptRA = false;
        DNS = [ "2a01:7e01:e003:8b00::53" ];
        Domains = [ "iot.club.muc.ccc.de" ];
        LLDP = true;
        EmitLLDP = true;
      };
    };

    netdevs."10-environment" = {
      netdevConfig = {
        Kind = "vlan";
        Name = "environment";
      };
      vlanConfig.Id = 2064;
    };

    networks."10-environment" = {
      matchConfig.Name = "environment";
      networkConfig = {
        Address = [
          "10.189.21.1/24"
          "2a01:7e01:e003:8bef::1/64"
          # legacy
          "83.133.178.65/26"
          # "2001:7f0:3003:beef::65/64"
        ];
        IPv6AcceptRA = false;
        DNS = [ "2a01:7e01:e003:8b00::53" ];
        Domains = [ "env.club.muc.ccc.de" ];
        LLDP = true;
        EmitLLDP = true;
      };
      routes = [
        {
          # git.muc.ccc.de
          Destination = "172.105.79.17/32";
        }
      ];
    };
    netdevs."10-access" = {
      netdevConfig = {
        Kind = "vlan";
        Name = "access";
      };
      vlanConfig.Id = 2400;
    };

    networks."10-access" = {
      matchConfig.Name = "access";
      networkConfig = {
        Address = [
          "10.189.42.1/23"
        ];
        DHCPPrefixDelegation = true;
        IPv6AcceptRA = false;
        DNS = [ "2a01:7e01:e003:8b00::53" ];
        Domains = [ "access.club.muc.ccc.de" ];
        LLDP = true;
        EmitLLDP = true;
      };
      dhcpPrefixDelegationConfig = {
        UplinkInterface = "ppp0";
        Announce = true;
        SubnetId = 0;
      };
      extraConfig = ''
        [DHCPPrefixDelegation]
        NFTSet=prefix:inet:firewall:mnet_ipv6_prefix
      '';
    };

    netdevs."10-mnet" = {
      netdevConfig = {
        Kind = "vlan";
        Name = "mnet";
      };
      vlanConfig.Id = 40;
    };

    networks."10-mnet" = {
      matchConfig.Name = "mnet";
      linkConfig.RequiredForOnline = false;
    };

    networks."10-ppp0" = {
      matchConfig.Name = "ppp0";
      linkConfig.RequiredForOnline = false;
      networkConfig = {
        KeepConfiguration = "static";
        DefaultRouteOnDevice = true;
        DHCP = "ipv6";
        IPv6AcceptRA = true;
      };
      ipv6AcceptRAConfig = {
        DHCPv6Client = "always";
        UseDNS = false;
        UseDomains = false;
      };
      dhcpV6Config = {
        RouteMetric = 5;
        PrefixDelegationHint = "::/56";
        UseDelegatedPrefix = true;
        WithoutRA = "solicit";
      };
      dhcpPrefixDelegationConfig = {
        Announce = false;
        UplinkInterface = ":self";
        SubnetId = 1;
        Assign = true;
        Token = "::1";
      };
    };

    netdevs."10-stammstrecke" = {
      netdevConfig = {
        Name = "stammstrecke";
        Kind = "wireguard";
        MTUBytes = "1400";
      };
      wireguardConfig = {
        PrivateKeyFile = config.sops.secrets.wireguard_private_key.path;
        ListenPort = 23425;
        RouteTable = 23;
      };
      wireguardPeers = [
        # stammstrecke
        {
          PublicKey = "gWT4W8ZKHZOYlplDkbPxlgz+l6yMcB6QoP8hgfJ94lA=";
          PresharedKeyFile = config.sops.secrets.wireguard_psk_stammstrecke.path;
          AllowedIPs = [
            "0.0.0.0/0"
            "::/0"
            "2a01:7e01:e003:8b00:ffff::1/128"
            "100.123.42.3/32"
            "2a01:7e01:e003:8b10::/64"
            "10.189.10.0/23"
          ];
          # Endpoint = "stammstrecke.muc.ccc.de:23425";
          Endpoint = "45.79.249.152:23425"; # FIXME
          PersistentKeepalive = 30;
        }
        # oaarchkatzl
        {
          PublicKey = "R/1NvJie5kVC3zsDMnWJaxRrVcWtYHL4kjrmjlLeyi4=";
          PresharedKeyFile = config.sops.secrets.wireguard_psk_oaarchkatzl.path;
          AllowedIPs = [
            "2a01:4f8:c012:1a6a::/64"
            "100.123.42.1/32"
          ];
          # oaarchkatzl.muc.ccc.de
          Endpoint = "5.75.248.200:23425";
          PersistentKeepalive = 30;
        }
      ];
    };
    networks."10-stammstrecke" = {
      matchConfig.Name = "stammstrecke";
      addresses = [
        {
          Address = "100.123.42.5/32";
          Peer = "100.123.42.3/32";
        }
        {
          Address = "100.123.42.5/32";
          Peer = "100.123.42.1/32";
        }
        {
          Address = "2a01:7e01:e003:8b00:ffff::5/128";
          Peer = "2a01:7e01:e003:8b00:ffff::1/128";
        }
        {
          Address = "2a01:7e01:e003:8b00:ffff::5/128";
          Peer = "2a01:4f8:c012:1a6a:ffff::1/128";
        }
        {
          Address = "2a01:7e01:e003:8b00::53/128";
        }
        {
          Address = "10.189.0.53/32";
        }
      ];
      networkConfig = {
        LLMNR = false;
        MulticastDNS = false;
      };
      routingPolicyRules = [
        # via local interfaces
        {
          Family = "ipv6";
          # marked traffic from mnet ipv6 and internal
          FirewallMark = "0x5";
          Table = "main";
          Priority = 5;
        }
        # via stammstrecke
        {
          From = "2a01:7e01:e003:8b00::/56";
          Table = 23;
        }
        {
          From = "172.105.79.17/32";
          Table = 23;
        }
      ];
      routes = [
        { Destination = "2a01:7e01:e003:8b10::/64"; }
        { Destination = "10.189.10.0/23"; }
      ];
    };
  };

  networking.nftables.ruleset = lib.mkBefore ''
    table inet firewall {
      set mnet_ipv6_prefix {
        type ipv6_addr
        flags interval
      }

      chain prerouting-mangle {
        type filter hook prerouting priority mangle; policy accept;
        ip6 saddr { 2a01:7e01:e003:8b00::/56 } ip6 daddr { 2a01:7e01:e003:8b00::/56 } counter meta mark set 0x5
        ip6 saddr { 2a01:7e01:e003:8b00::/56 } ip6 daddr @mnet_ipv6_prefix counter meta mark set 0x5
      }
    }
  '';

  networking.nftables.chains =
    let
      rejectRule = location: {
        after = lib.mkForce [ "veryLate" ];
        before = lib.mkForce [ "end" ];
        rules = lib.singleton ''
          counter log prefix "rejected[${location}]: " reject with icmpx type admin-prohibited
        '';
      };
    in
    {
      postrouting.mnet-nat = {
        rules = lib.singleton ''
          ip saddr 10.189.0.0/16 oifname { ppp0 } masquerade
          ip saddr 83.133.178.0/23 oifname { ppp0 } masquerade
        '';
      };
      forward.mss-pmtu = {
        after = lib.mkForce [ "veryEarly" ];
        before = [ "conntrack" ];
        rules = lib.singleton ''
          tcp flags syn tcp option maxseg size set rt mtu
          oifname { stammstrecke } tcp flags syn tcp option maxseg size set 1340
          iifname { stammstrecke } tcp flags syn tcp option maxseg size set 1340
        '';
      };

      output.mark-mnet = {
        after = lib.mkForce [ "veryEarly" ];
        before = [ "conntrack" ];
        rules = lib.singleton ''
          type route hook output priority filter; policy accept
          ip6 saddr { 2a01:7e01:e003:8b00::/56 } ip6 daddr { 2a01:7e01:e003:8b00::/56 } counter meta mark set 0x5
          ip6 saddr { 2a01:7e01:e003:8b00::/56 } ip6 daddr @mnet_ipv6_prefix counter meta mark set 0x5
        '';
      };

      forward.reject = rejectRule "forward";
      input.reject = rejectRule "input";
    };

  networking.nftables.firewall = {
    enable = true;
    snippets = {
      nnf-common.enable = true;
      nnf-drop.enable = false; # reject instead
      nnf-icmp = {
        ipv6Types = [
          "echo-request"
          "nd-router-solicit"
          "nd-router-advert"
          "nd-neighbor-solicit"
          "nd-neighbor-advert"
          # allow multicast
          "mld-listener-query"
          "mld-listener-report"
          "mld-listener-reduction"
          "mld2-listener-report"
        ];
      };
    };
    zones.uplink = {
      interfaces = [
        "ppp0"
        "stammstrecke"
      ];
    };
    zones.club = {
      interfaces = [
        "access"
        "environment"
        "mgmt"
      ];
    };
    zones.clubvpn = {
      ipv6Addresses = [ "2a01:7e01:e003:8b10::/62" ];
      ipv4Addresses = [ "10.189.8.0/21" ];
    };
    zones.iot = {
      interfaces = [ "iot" ];
    };
    zones.environment = {
      ipv6Addresses = [ "2a01:7e01:e003:8bef::/64" ];
    };
    zones.oaarchkatzl = {
      ipv6Addresses = [ "2a01:4f8:c012:1a6a::/64" ];
      ipv4Addresses = [ "100.123.42.1/32" ];
    };
    zones.brezn = {
      ipv6Addresses = [ "2a01:7e01:e003:8bef:74a3:23ff:fead:57e3/128" ];
    };
    zones.omicron = {
      ipv6Addresses = [ "2a01:7e01:e003:8bef:da5e:d3ff:fe29:af33/128" ];
    };
    zones.space = {
      ipv6Addresses = [
        "2a01:7e01:e003:8bef:fc5e:dff:fe43:2d5f/128"
      ];
    };
    zones.iridium = {
      ipv6Addresses = [
        "2a01:7e01:e003:8bef:b62e:99ff:fe97:fa8e/128"
      ];
    };
    zones.git = {
      ipv6Addresses = [ "2a01:7e01:e003:8bef:90dd:4fff:fe7b:e3eb/128" ];
      ipv4Addresses = [ "172.105.79.17/32" ];
    };
    rules."icmp" = {
      extraLines =
        let
          cfg = config.networking.nftables.firewall.snippets.nnf-icmp;
        in
        lib.mkForce [
          "ip6 nexthdr icmpv6 icmpv6 type { ${lib.concatStringsSep ", " cfg.ipv6Types} } accept"
          "ip6 nexthdr icmpv6 counter log prefix \"dropped icmpv6: \" drop"
          "ip protocol icmp icmp type { ${lib.concatStringsSep ", " cfg.ipv4Types} } accept"
          "ip protocol icmp counter log prefix \"dropped icmp: \" drop"
        ];
    };
    rules."wireguard" = {
      from = [ "uplink" ];
      to = [ "fw" ];
      allowedUDPPorts = [
        23425
      ];
    };
    rules."dns-club" = {
      from = [
        "club"
        "clubvpn"
        "iot"
      ];
      to = [ "fw" ];
      allowedUDPPorts = [
        53
        443 # doh
      ];
      allowedTCPPorts = [
        53
        853 # dot
        443 # doh
        8053 # metrics
      ];
    };
    rules."dns-from-uplink" = {
      from = [ "uplink" ];
      to = [ "brezn" ];
      allowedUDPPorts = [ 53 ];
      allowedTCPPorts = [ 53 ];
    };
    rules."dhcp-server" = {
      from = [
        "club"
        "iot"
      ];
      to = [ "fw" ];
      allowedUDPPorts = [
        67
        68
      ];
    };
    rules."dhcpv6-server" = {
      from = [
        "club"
        "iot"
      ];
      to = [ "fw" ];
      allowedUDPPorts = [ 547 ];
      allowedTCPPorts = [ 547 ];
    };
    rules."prometheus-node-exporter" = {
      from = [ "club" ];
      to = [ "fw" ];
      allowedTCPPorts = [ 9100 ];
    };
    rules."club-internal" = {
      from = [
        "club"
        "oaarchkatzl"
        "clubvpn"
      ];
      to = [
        "club"
        "iot"
        "oaarchkatzl"
        "clubvpn"
      ];
      verdict = "accept";
    };
    rules."club-to-uplink" = {
      from = [ "club" ];
      to = [ "uplink" ];
      verdict = "accept";
    };
    rules."iot-drop" = {
      from = [ "iot" ];
      to = [ "uplink" ];
      extraLines = [
        "counter drop"
      ];
    };
    rules."uplink-to-club-icmp" = {
      from = [ "uplink" ];
      to = [ "club" ];
      extraLines = [
        "ip6 nexthdr icmpv6 icmpv6 type { echo-request } accept"
        "ip protocol icmp icmp type { echo-request } accept"
      ];
    };
    rules."uplink-to-club-ssh" = {
      from = [ "uplink" ];
      to = [
        "fw"
        "git"
        "iridium"
        "space"
        "omicron"
      ];
      allowedTCPPorts = [ 22 ];
    };
    rules."club-http" = {
      from = [
        "uplink"
        "club"
      ];
      to = [
        "fw"
        "brezn"
        "git"
        "space"
        "omicron"
      ];
      allowedTCPPorts = [
        80
        443
      ];
      allowedUDPPorts = [ 443 ];
    };
  };

  muccc.recursor = {
    enable = true;
    listenIps = [
      "10.189.23.1" # FIXME
      "10.189.0.53"
      "[2a01:7e01:e003:8b00::53]"
    ];
    webmgmtListenIps = [
      "10.189.23.1"
      "2a01:7e01:e003:8b23::1"
    ];
  };

  services.dnsmasq = {
    enable = true;
    resolveLocalQueries = false;
    settings = {
      dhcp-range = [
        "::1:1,::1:ffff,constructor:access,slaac,ra-names"
        "10.189.42.10,10.189.43.250,24h"
        "::1:1,::1:ffff,constructor:mgmt,slaac,ra-names"
        "10.189.23.10,10.189.23.250,24h"
        "::1:1,::1:ffff,constructor:iot,slaac,ra-names"
        "10.189.16.10,10.189.16.250,24h"
        "::1:1,::1:ffff,constructor:environment,slaac,ra-names"
        "10.189.21.10,10.189.21.250,24h"
      ];
      domain = [
        "club.muc.ccc.de"
        "access.club.muc.ccc.de,access"
        "mgmt.club.muc.ccc.de,mgmt"
        "env.club.muc.ccc.de,environment"
        "iot.club.muc.ccc.de,iot"
      ];
      auth-server = [
        "club.muc.ccc.de"
      ];
      auth-zone = [
        "access.club.muc.ccc.de,access"
        "mgmt.club.muc.ccc.de,mgmt"
        "env.club.muc.ccc.de,environment"
        "iot.club.muc.ccc.de,iot"
      ];
      interface = [
        "access"
        "mgmt"
        "iot"
        "environment"
      ];
      bind-dynamic = true;
      enable-ra = true;
      dhcp-rapid-commit = true;
      dhcp-fqdn = true;
      dhcp-authoritative = true;
      no-resolv = true;
      listen-address = [
        "127.0.0.1"
        "::1"
      ];
      port = 5553;
      address = [
        #"/wifi.mgmt.club.muc.ccc.de/2a01:7e01:e003:8b23::9"
        "/wifi.mgmt.club.muc.ccc.de/10.189.23.9"
        # nginx.club.muc.ccc.de
        "/cache.club.muc.ccc.de/2a01:7e01:e003:8bef:5ce0:1eff:fe54:911f"
        "/cache.club.muc.ccc.de/10.189.21.176"
      ];
      dhcp-option = [
        "option:dns-server,10.189.0.53"
        "option6:dns-server,[2a01:7e01:e003:8b00::53]"
      ];
    };
  };

  muccc.acme-dns.certs = {
    "resolver.${config.networking.fqdn}" = "acme-dns-transrapid";
  };

  security.acme.certs = {
    "resolver.${config.networking.fqdn}" = {
      postRun = ''
        systemctl restart kresd@*
      '';
    };
  };

  services.nginx = {
    # knot-resolver is also listening on DNS IPs
    # virtualHosts."clubvpn.${config.networking.fqdn}".listenAddresses = [
    #   "[2a01:7e01:e003:8b23::1]"
    #   "10.189.23.1"
    # ];
  };

  muccc.clubvpn = {
    enable = false; # FIXME: firewall fnord via fritzbox
    cidr = "10.189.8.0/23";
    cidrv6 = "2a01:7e01:e003:8b11::/64";
  };

  services.pppd = {
    enable = true;
    peers.mnet.config = ''
      plugin pppoe.so mnet
      ifname ppp0
      file "${config.sops.secrets.ppp_mnet.path}"
      debug
      persist
      maxfail 0
      holdoff 5
      noipdefault
      defaultroute
      defaultroute-metric 512
      lcp-echo-failure 3
      lcp-echo-interval 10
      mtu 1492
      mru 1492

      noaccomp nobsdcomp nodeflate nopcomp novj novjccomp
      refuse-mschap refuse-mschap-v2 refuse-eap refuse-pap
    '';
  };

  systemd.services."systemd-networkd".environment.SYSTEMD_LOG_LEVEL = "debug";
}
