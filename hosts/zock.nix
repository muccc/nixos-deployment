{
  system.stateVersion = "24.05";

  networking = {
    hostName = "zock";
    domain = "club.muc.ccc.de";

    firewall.allowedTCPPorts = [ ];
    firewall.allowedUDPPorts = [ 30000 ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "2a:e1:d2:38:a6:79";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  services.satisfactory.enable = false;
}
