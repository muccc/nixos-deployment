{
  modulesPath,
  lib,
  config,
  pkgs,
  ...
}:
{
  system.stateVersion = "24.11";

  services.postgresql.package = pkgs.postgresql_17;

  sops = {
    secrets = {
      wireguard_private_key = {
        owner = "systemd-network";
      };
      wireguard_psk = {
        owner = "systemd-network";
      };
    };
  };

  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  boot.initrd.availableKernelModules = [
    "virtio_pci"
    "virtio_scsi"
    "ahci"
    "sd_mod"
  ];
  boot.kernelParams = [ "console=ttyS0,19299n8" ];
  boot.loader.grub.extraConfig = ''
    serial --speed-19200 --unit=0 --word=8 --parity=no --stop=1;
    terminal_input serial;
    terminal_output serial
  '';
  boot.loader.grub.forceInstall = true;
  boot.loader.grub.device = lib.mkForce "nodev";
  boot.loader.timeout = 10;

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/e007a90a-95c7-4ffd-813d-f1718ecb6786";
    fsType = "ext4";
    options = [ "noatime" ];
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/f1408ea6-59a0-11ed-bc9d-525400000001"; }
  ];

  networking = {
    domain = "muc.ccc.de";
    firewall = {
      extraCommands = ''
        ip46tables -A FORWARD -i clubvpn -o muccc -j ACCEPT
        ip46tables -A FORWARD -i clubvpn -j DROP
      '';
      allowedUDPPorts = [
        23425
      ];
    };
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "f2:3c:95:8d:46:b5";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [
        "45.79.249.152/24"
        "2a01:7e01::f03c:95ff:fe8d:46b5/128"
      ];
      Gateway = [
        "45.79.249.1"
        "fe80::1"
      ];
      DNS = [
        "2a01:7e01::5"
        "2a01:7e01::c"
        "139.162.134.5"
        "139.162.133.5"
      ];
    };
    routes = [
      {
        # weird linode routing
        Destination = "172.105.79.0/24";
      }
    ];
  };

  boot.kernel.sysctl = {
    "net.ipv4.ip_forward" = lib.mkForce "1";
    "net.ipv6.conf.all.forwarding" = lib.mkForce "1";
  };

  systemd.network.netdevs."10-muccc" = {
    netdevConfig = {
      Name = "muccc";
      Kind = "wireguard";
      MTUBytes = "1400";
    };
    wireguardConfig = {
      PrivateKeyFile = config.sops.secrets.wireguard_private_key.path;
      ListenPort = 23425;
      RouteTable = "main";
    };
    wireguardPeers = [
      # garching
      {
        PublicKey = "pqfhiGTTaW+kniwL7bYQBTfciJ3BNkN21euvpedl8Fg=";
        PresharedKeyFile = config.sops.secrets.wireguard_psk.path;
        AllowedIPs = [
          "100.123.42.2/32"
          "2a01:7e01:e003:8b00:ffff::2/128"
        ];
        PersistentKeepalive = 60;
      }
      # transrapid
      {
        PublicKey = "LBHy0Yzf4gH2f5wwA+FBDIG3NjPYdR/8nyFDju28K3c=";
        PresharedKeyFile = config.sops.secrets.wireguard_psk.path;
        AllowedIPs = [
          "100.123.42.5/32"
          "2a01:7e01:e003:8b00:ffff::5/128"
          # routed
          "172.105.79.17/32"
          "10.189.0.0/16"
          "2a01:7e01:e003:8b00::/56"
          # legacy environment
          "2001:7f0:3003:beef::/64"
          "83.133.178.64/26"
        ];
      }
    ];
  };
  systemd.network.networks."10-muccc" = {
    matchConfig.Name = "muccc";
    address = [
      "2a01:7e01:e003:8b00:ffff::1/128"
      "100.123.42.3/32"
    ];
    networkConfig = {
      LLMNR = false;
      MulticastDNS = false;
    };
  };

  muccc.clubvpn = {
    enable = true;
    cidr = "10.189.10.0/23";
    cidrv6 = "2a01:7e01:e003:8b10::/64";
  };

  muccc.matrix.enable = true;

  services.longview = {
    enable = true;
    apiKeyFile = "/var/lib/longview/apiKeyFile";
  };
}
