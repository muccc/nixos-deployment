{
  system.stateVersion = "24.11";

  muccc.qemu-guest.uefi = true;

  imports = [
    ../profiles/actions-runner.nix
  ];

  sops = {
    secrets.forgejo_registration_token = { };
  };

  networking = {
    firewall.allowedTCPPorts = [
      8093
      9090
    ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "DE:D3:39:B8:FC:74";
    linkConfig.Name = "upl0nk";
  };

  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  virtualisation.docker = {
    enable = true;
    listenOptions = [
      "/run/docker.sock"
      "127.0.0.1:2375"
    ];
  };
}
