{ config, pkgs, ... }:
{
  system.stateVersion = "23.05";

  sops = {
    secrets.netbox_initial_password = {
      owner = "netbox";
    };
    secrets.netbox_auth_client_id = {
      owner = "netbox";
    };
    secrets.netbox_auth_client_secret = {
      owner = "netbox";
    };
    secrets.netbox_proxbox = {
      owner = "netbox";
    };
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "5e:a4:63:66:50:f0";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  services.netbox = {
    enable = true;
    package = pkgs.netbox.overrideAttrs (attrs: {
      installPhase =
        attrs.installPhase
        + ''
          ln -sf ${../static/netbox/custom_pipeline.py} $out/opt/netbox/netbox/netbox/custom_pipeline.py
        '';
    });
    listenAddress = "[::1]";
    secretKeyFile = config.sops.secrets.netbox_initial_password.path;
    plugins =
      python3Packages: with python3Packages; [
        python-jose
        (buildPythonPackage rec {
          pname = "netbox-proxbox";
          version = "0.0.5-unstable-2024-11-26";

          src = pkgs.fetchFromGitHub {
            owner = "netdevopsbr";
            repo = pname;
            rev = "59bf4dd635abf327c28b46c07264c2ec80d8a16b";
            hash = "sha256-Zt90e0uFW8WK9CvP1XFZoqP2IFdxwDMfX/27O8KcqQg=";
          };

          postPatch = ''
            substituteInPlace setup.py \
              --replace "'poetry'," "" \
              --replace "'invoke'," ""
          '';

          nativeBuildInputs = [
            poetry-core
            pythonRelaxDepsHook
          ];
          propagatedBuildInputs = [
            paramiko
            pynetbox
            proxmoxer
          ];

          doCheck = false;
        })
      ];
    settings = {
      PLUGINS = [ "netbox_proxbox" ];
      PLUGINS_CONFIG = {
        netbox_proxbox = {
          proxmox = {
            domain = "pve.club.muc.ccc.de";
            http_port = 443;
            ssl = true;
          };

          netbox = {
            domain = "localhost";
            http_port = 8001;
            ssl = false;
            settings = {
              virtualmachine_role_id = 0;
              node_role_id = 0;
              site_id = 0;
            };
          };
        };
      };

      LOGGING = {
        version = 1;
        disable_existing_loggers = false;
        handlers = {
          console = {
            class = "logging.StreamHandler";
          };
        };

        root = {
          handlers = [ "console" ];
          level = "INFO";
        };
      };
    };

    extraConfig = ''
      # wtf
      import os
      PLUGINS_CONFIG["netbox_proxbox"]["proxmox"]["user"] = os.getenv("PROXBOX_PVE_USERNAME")
      PLUGINS_CONFIG["netbox_proxbox"]["proxmox"]["password"] = os.getenv("PROXBOX_PVE_PASSWORD")
      PLUGINS_CONFIG["netbox_proxbox"]["proxmox"]["token"] = {}
      PLUGINS_CONFIG["netbox_proxbox"]["proxmox"]["token"]["name"] = os.getenv("PROXBOX_PVE_TOKEN_NAME")
      PLUGINS_CONFIG["netbox_proxbox"]["proxmox"]["token"]["value"] = os.getenv("PROXBOX_PVE_TOKEN_VALUE")
      PLUGINS_CONFIG["netbox_proxbox"]["netbox"]["token"] = os.getenv("PROXBOX_NETBOX_TOKEN")

      # NetBox settings
      REMOTE_AUTH_ENABLED = True
      REMOTE_AUTH_BACKEND = 'social_core.backends.open_id_connect.OpenIdConnectAuth'

      # python-social-auth configuration
      SOCIAL_AUTH_OIDC_ENDPOINT = 'https://auth.muc.ccc.de/application/o/netbox'
      with open('${config.sops.secrets.netbox_auth_client_id.path}', "r") as id_file:
        SOCIAL_AUTH_OIDC_KEY = id_file.readline()
      with open('${config.sops.secrets.netbox_auth_client_secret.path}', "r") as secret_file:
        SOCIAL_AUTH_OIDC_SECRET = secret_file.readline()
      LOGOUT_REDIRECT_URL = 'https://auth.muc.ccc.de/application/o/netbox/end-session/'

      SOCIAL_AUTH_PIPELINE = (
          ###################
          # Default pipelines
          ###################

          # Get the information we can about the user and return it in a simple
          # format to create the user instance later. In some cases the details are
          # already part of the auth response from the provider, but sometimes this
          # could hit a provider API.
          'social_core.pipeline.social_auth.social_details',

          # Get the social uid from whichever service we're authing thru. The uid is
          # the unique identifier of the given user in the provider.
          'social_core.pipeline.social_auth.social_uid',

          # Verifies that the current auth process is valid within the current
          # project, this is where emails and domains whitelists are applied (if
          # defined).
          'social_core.pipeline.social_auth.auth_allowed',

          # Checks if the current social-account is already associated in the site.
          'social_core.pipeline.social_auth.social_user',

          # Make up a username for this person, appends a random string at the end if
          # there's any collision.
          'social_core.pipeline.user.get_username',

          # Send a validation email to the user to verify its email address.
          # Disabled by default.
          # 'social_core.pipeline.mail.mail_validation',

          # Associates the current social details with another user account with
          # a similar email address. Disabled by default.
          # 'social_core.pipeline.social_auth.associate_by_email',

          # Create a user account if we haven't found one yet.
          'social_core.pipeline.user.create_user',

          # Create the record that associates the social account with the user.
          'social_core.pipeline.social_auth.associate_user',

          # Populate the extra_data field in the social record with the values
          # specified by settings (and the default ones like access_token, etc).
          'social_core.pipeline.social_auth.load_extra_data',

          # Update the user record with any changed info from the auth service.
          'social_core.pipeline.user.user_details',

          ###################
          # Custom pipelines
          ###################
          # Set authentik Groups
          'netbox.custom_pipeline.add_groups',
          'netbox.custom_pipeline.remove_groups',
          # Set Roles
          'netbox.custom_pipeline.set_roles'
      )
    '';
  };

  systemd.services.netbox.serviceConfig.EnvironmentFile = config.sops.secrets.netbox_proxbox.path;
  systemd.services.netbox-migration.serviceConfig.EnvironmentFile =
    config.sops.secrets.netbox_proxbox.path;
  systemd.services.netbox-housekeeping.serviceConfig.EnvironmentFile =
    config.sops.secrets.netbox_proxbox.path;
  systemd.services.netbox-rq.serviceConfig.EnvironmentFile = config.sops.secrets.netbox_proxbox.path;
  systemd.services.netbox.environment = {
    GUNICORN_CMD_ARGS = "--threads 2 --timeout 300";
  };

  muccc.acme-dns.certs = {
    "netbox.club.muc.ccc.de" = "acme-dns-netbox";
  };

  services.nginx = {
    group = "netbox";
    enable = true;
    virtualHosts."netbox.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations = {
        "/" = {
          proxyPass = "http://localhost:8001";
        };
        "/static" = {
          root = config.services.netbox.dataDir;
        };
      };
    };
  };
}
