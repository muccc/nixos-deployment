{
  system.stateVersion = "23.05";

  networking = {
    hostName = "forgejo";
    domain = "club.muc.ccc.de";
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "92:dd:4f:7b:e3:eb";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
      Address = [ "172.105.79.17/32" ];
    };
    routes = [
      {
        Destination = "83.133.178.70/26";
      }
    ];
  };

  muccc.git.enable = true;
}
