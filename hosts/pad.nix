{ config, ... }:
{
  system.stateVersion = "23.05";

  sops = {
    secrets.hedgedoc_env = {
      owner = "hedgedoc";
    };
  };

  networking = {
    hostName = "pad";
    domain = "club.muc.ccc.de";
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "6E:23:5D:5E:DF:0B";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "hedgedoc" ];
    ensureUsers = [
      {
        name = "hedgedoc";
        ensureDBOwnership = true;
      }
    ];
  };

  services.hedgedoc = {
    enable = true;
    settings = {
      allowFreeURL = true;
      allowAnonymous = true;
      allowAnonymousEdits = true;
      allowPDFExport = true;
      allowEmailRegister = false;
      email = false;
      domain = "pad.muc.ccc.de";
      allowOrigin = [ "pad.muc.ccc.de" ];
      protocolUseSSL = true;
      db = {
        dialect = "postgres";
        host = "/run/postgresql";
        database = "hedgedoc";
      };
      sessionLife = 90 * 24 * 60 * 60 * 1000; # 90d
      csp.directives = {
        frame-ancestors = "https://webex.muc.ccc.de";
      };
    };
    environmentFile = config.sops.secrets.hedgedoc_env.path;
  };

  muccc.acme-dns.certs = {
    "pad.club.muc.ccc.de" = "acme-dns-pad";
    "pad.muc.ccc.de" = "acme-dns-pad";
  };

  services.nginx = {
    enable = true;
    virtualHosts."pad.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/".return = "302 https://pad.muc.ccc.de";
    };
    virtualHosts."pad.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations = {
        "/" = {
          proxyPass = "http://localhost:3000";
        };

        "/socket.io/" = {
          proxyPass = "http://localhost:3000";
          proxyWebsockets = true;
        };
      };
    };
  };
}
