{
  lib,
  config,
  inputs,
  pkgs,
  ...
}:

let
  muccc-api = inputs.muccc-api.packages.${config.nixpkgs.system}.default;
in
{
  system.stateVersion = "23.05";

  sops = {
    secrets = {
      api_env = { };
      matrix_env = { };
    };
  };

  boot = {
    supportedFilesystems = [ "xfs" ];
  };

  fileSystems = {
    "/" = lib.mkIf config.muccc.targetDeploy {
      device = "/dev/vda2";
      fsType = lib.mkForce "xfs";
      options = [
        "noatime"
        "nodiratime"
        "logbufs=8"
      ];
    };
  };

  swapDevices = [ { device = "/dev/vda3"; } ];

  networking = {
    hostName = "nixbus";
    domain = "club.muc.ccc.de";
    firewall = {
      allowedTCPPorts = [ 22 ];
      allowedUDPPorts = [
        2080 # schleusenstate multicast
      ];
    };
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "00:20:91:f9:b9:f9";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  muccc.acme-dns.certs = {
    "nixbus.club.muc.ccc.de" = "acme-dns-nixbus";
    "api.muc.ccc.de" = "acme-dns-nixbus";
    "labels.club.muc.ccc.de" = "acme-dns-nixbus";
    "flipdot.club.muc.ccc.de" = "acme-dns-nixbus";
  };

  services.nginx = {
    enable = true;
    virtualHosts."nixbus.club.muc.ccc.de" = {
      enableACME = true;
      addSSL = true;
      acmeRoot = null;
      locations."/".return = "204";
    };
    virtualHosts."api.muc.ccc.de" = {
      enableACME = true;
      addSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://[::1]:8020";
        proxyWebsockets = true;
      };
      locations."/wiki_kalender.ics".return = "301 https://api.muc.ccc.de/events/all.ics";
      locations."/wiki_kalender_public.ics".return = "301 https://api.muc.ccc.de/events/public.ics";
    };
    virtualHosts."labels.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/".proxyPass = "http://${config.services.zpl-server.listen}";
    };
    virtualHosts."flipdot.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/".root = pkgs.callPackage (inputs.flipdot-image-editor + "/package.nix") { };
    };
  };

  systemd.services.muccc-api = {
    wantedBy = [ "multi-user.target" ];
    description = "MuCCC API";
    serviceConfig = {
      ExecStart = "${muccc-api}/bin/muccc-api";
      EnvironmentFile = [ config.sops.secrets.api_env.path ];
      Restart = "always";
      DynamicUser = true;
      PrivateTmp = true;
    };
  };

  systemd.services.muccc-api-matrix-bot = {
    wantedBy = [ "multi-user.target" ];
    description = "MuCCC IRC bot schleuse";
    environment = {
      USER_ID = "@q:muc.ccc.de";
      MAIN_ROOM_ID = "!IsSbyywyWZXTqCYrVj:hackint.org"; # #muccc:hackint.org
      API_BASE_URL = "https://api.muc.ccc.de";
    };
    serviceConfig = {
      ExecStart = "${muccc-api}/bin/muccc-matrix-bot";
      EnvironmentFile = [ config.sops.secrets.matrix_env.path ];
      Restart = "always";
      DynamicUser = true;
      PrivateTmp = true;
      StateDirectory = "muccc-api-matrix-bot";
    };
  };

  systemd.services.muccc-flipdots = {
    wantedBy = [ "multi-user.target" ];
    description = "MuCCC Flipdots Update";
    serviceConfig = {
      ExecStart = "${muccc-api}/bin/muccc-flipdots";
      Restart = "always";
      DynamicUser = true;
      PrivateTmp = true;
    };
  };

  services.zpl-server = {
    enable = true;
    settings = {
      labels = {
        "51mm" = {
          dimensions = {
            width = 51.0;
            height = 51.0;
            margin_left = 1.0;
            margin_right = 1.0;
            margin_top = 1.0;
            margin_bottom = 1.0;
          };
        };
      };
      printers = {
        gx430t = {
          label = "51mm";
          addr = "192.168.179.111:9100";
          display_name = "Porkhy";
        };
      };
    };
  };
}
