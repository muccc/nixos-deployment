{
  config,
  inputs,
  pkgs,
  lib,
  ...
}:
{
  imports = [ inputs.impermanence.nixosModules.impermanence ];

  system.stateVersion = "23.05";

  muccc.qemu-guest.enable = false;

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "84:7b:eb:eb:20:dd";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = "yes";
      LLMNR = false;
    };
    dhcpV4Config = {
      ClientIdentifier = "mac";
    };
  };

  environment.systemPackages = [ pkgs.picocom ];

  services.nginx.virtualHosts."drugga.club.muc.ccc.de" = {
    forceSSL = true;
    useACMEHost = "drugga.club.muc.ccc.de";
    locations."/".proxyPass = "http://localhost:5000";
    locations."/".proxyWebsockets = true;
  };

  services.octoprint = {
    enable = true;
    # Typically accessed from club access net, which is separate from
    # the environment net in which drugga sits and has a dynamic
    # prefix (otherwise I'd use ipCheck.trustedSubnets)
    extraConfig.server.ipCheck.enabled = false;
  };

  nixpkgs.overlays = [
    (_self: super: {
      octoprint = super.octoprint.override {
        packageOverrides = (
          _pyself: pysuper: {
            pytest-httpbin = pysuper.pytest-httpbin.overrideAttrs (_o: {
              doInstallCheck = false;
            });
            httpcore = pysuper.httpcore.overrideAttrs (_o: {
              doInstallCheck = false;
            });
          }
        );
      };
    })
  ];

  networking.firewall.allowedTCPPorts = [
    80
    443
    7125
  ];

  muccc.acme-dns.certs."drugga.club.muc.ccc.de" = "acme-dns-drugga";
  security.acme.certs."drugga.club.muc.ccc.de".group = "nginx";

  services.fluidd = {
    enable = true;
  };

  boot.loader.systemd-boot.enable = true;

  fileSystems."/" = lib.mkIf config.muccc.targetDeploy (
    lib.mkForce {
      fsType = "tmpfs";
      device = "tmpfs";
      options = [ "mode=0755" ];
    }
  );

  users.users.root = inputs.self.lib.humanoids.authorizedKeysMixinForUser "linus";

  boot.initrd.availableKernelModules = [
    "sdhci-pci"
    "mmc_block"
  ];

  environment.persistence."/local" = {
    files = [
      "/etc/machine-id"
      "/etc/ssh/ssh_host_ed25519_key"
      "/etc/ssh/ssh_host_rsa_key"
      "/root/.bash_history"
    ];
    directories = [ "/var/lib/octoprint" ];
  };

  fileSystems."/local" = {
    fsType = "btrfs";
    device = "/dev/disk/by-uuid/6a799315-064b-47bb-9bb0-1e70be13a8ba";
    neededForBoot = true;
    options = [
      "compress=zstd"
      "noatime"
      "discard=async"
      "subvol=/local"
    ];
  };
  fileSystems."/nix" = {
    fsType = "btrfs";
    device = "/dev/disk/by-uuid/6a799315-064b-47bb-9bb0-1e70be13a8ba";
    neededForBoot = true;
    options = [
      "compress=zstd"
      "noatime"
      "discard=async"
      "subvol=/nix"
    ];
  };
  fileSystems."/boot" = {
    fsType = "vfat";
    device = "/dev/disk/by-uuid/4326-5FFB";
  };
}
