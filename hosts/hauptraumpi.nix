{ config, ... }:
{
  system.stateVersion = "24.11";

  muccc.rpi.enable = config.muccc.targetDeploy;

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "dc:a6:32:6a:9f:55";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = "yes";
      LLMNR = false;
    };
    dhcpV4Config = {
      ClientIdentifier = "mac";
    };
  };

  muccc.multimedia = {
    enable = true;
    username = "hauptraum";
  };
}
