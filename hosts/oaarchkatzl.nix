{
  config,
  pkgs,
  ...
}:
{
  system.stateVersion = "24.05";

  sops = {
    secrets = {
      coturn_auth_secret = {
        owner = "turnserver";
      };
      prosody_turn_secret = {
        owner = "prosody";
      };
      wireguard_private_key = {
        owner = "systemd-network";
      };
      wireguard_psk = {
        owner = "systemd-network";
      };
    };
  };

  muccc.hcloud.aarch64.enable = config.muccc.targetDeploy;

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/730A-DC91";
    fsType = "vfat";
  };

  networking = {
    hostName = "oaarchkatzl";
    domain = "muc.ccc.de";
    firewall = {
      allowedTCPPorts = [ 53 ];
      allowedUDPPorts = [
        53
        23425
      ];
    };
  };

  services.cloud-init.enable = false;
  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "96:00:02:6b:e1:52";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    address = [
      "5.75.248.200/32"
      "2a01:4f8:c012:1a6a::/128"
      "2a01:4f8:c012:1a6a::443/128"
    ];
    gateway = [ "fe80::1" ];
    routes = [
      {
        Gateway = "172.31.1.1";
        GatewayOnLink = true;
      }
    ];
    dns = [ "8.8.8.8" ];
  };

  systemd.network.netdevs."10-muccc" = {
    netdevConfig = {
      Name = "muccc";
      Kind = "wireguard";
      MTUBytes = "1400";
    };
    wireguardConfig = {
      PrivateKeyFile = config.sops.secrets.wireguard_private_key.path;
      ListenPort = 23425;
      RouteTable = "main";
    };
    wireguardPeers = [
      # garching
      {
        PublicKey = "pqfhiGTTaW+kniwL7bYQBTfciJ3BNkN21euvpedl8Fg=";
        PresharedKeyFile = config.sops.secrets.wireguard_psk.path;
        AllowedIPs = [
          "100.123.42.2/32"
          "2a01:4f8:c012:1a6a:ffff::2/128"
        ];
        PersistentKeepalive = 60;
      }
      # transrapid
      {
        PublicKey = "LBHy0Yzf4gH2f5wwA+FBDIG3NjPYdR/8nyFDju28K3c=";
        PresharedKeyFile = config.sops.secrets.wireguard_psk.path;
        AllowedIPs = [
          "100.123.42.5/32"
          "2a01:7e01:e003:8b00:ffff::5/128"
          "2a01:7e01:e003:8b00::/56"
          "2001:7f0:3003:beef::/64"
          "83.133.178.64/26"
        ];
      }
    ];
  };
  systemd.network.networks."10-muccc" = {
    matchConfig.Name = "muccc";
    address = [
      "2a01:4f8:c012:1a6a:ffff::1/128"
      "100.123.42.1/32"
    ];
    networkConfig = {
      LLMNR = false;
      MulticastDNS = false;
    };
  };

  muccc.webex.enable = true;
  muccc.fasel.enable = true;
  muccc.wiki.enable = true;

  services.nginx = {
    enable = true;

    virtualHosts."oaarchkatzl.muc.ccc.de" = {
      serverAliases = [ "oaarchkatzl-http.muc.ccc.de" ];
      enableACME = true;
      forceSSL = true;
      default = true;
      locations."/".return = "204 'no content'";
    };

    virtualHosts."neu.www.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/".root = "/srv/www/neu.www.muc.ccc.de";
    };

    virtualHosts."api.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://nixbus.club.muc.ccc.de";
        proxyWebsockets = true;
      };
    };

    virtualHosts."cache.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        # proxyPass = "http://gitlab-runner.club.muc.ccc.de:8080";
        return = "204 'no content'";
        extraConfig = ''
          client_max_body_size 2G;
          proxy_request_buffering off;
          proxy_connect_timeout 120s;
          proxy_read_timeout 180s;
          proxy_send_timeout 180s;
        '';
      };
    };

    virtualHosts."auth.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://auth.club.muc.ccc.de";
        proxyWebsockets = true;
      };
    };

    virtualHosts."pad.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://pad.club.muc.ccc.de";
        proxyWebsockets = true;
      };
    };

    virtualHosts."otrs.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://otrs.club.muc.ccc.de";
        extraConfig = ''
          proxy_ssl_verify off;
        '';
      };
    };

    # virtualHosts."druck.muc.ccc.de" = {
    #   forceSSL = true;
    #   enableACME = true;
    #   locations."/" = {
    #     proxyPass = "http://drugga.club.muc.ccc.de";
    #     root = pkgs.writeTextDir "druck-errors/502.html" ''
    #       drugga (thin client in the lab attached to the 3D printer) seems to be down, try turning it on?
    #     '';
    #     extraConfig = ''
    #       error_page 502 = /502.html;
    #       # only allow access from club net
    #       satisfy any;
    #       allow 2001:7f0:3003:beef::/64;
    #       deny all;
    #     '';
    #   };
    # };

    virtualHosts."cloud.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://nextcloud.club.muc.ccc.de";
        proxyWebsockets = true;
      };
    };

    virtualHosts."cloud-office.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://nextcloud.club.muc.ccc.de";
        proxyWebsockets = true;
      };
    };

    virtualHosts."acme-dns.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."~ ^/$".return = "204";
      locations."/".proxyPass = "http://[::1]:8053";
    };

    virtualHosts."nixos-passthru-cache" = {
      acmeRoot = null;
    };
  };

  muccc.acme-dns.certs = {
    "cache.club.muc.ccc.de" = "acme-dns-cache";
  };

  muccc.nix-cache = {
    enable = true;
  };

  nix.settings.trusted-users = [ "www-data" ];

  users.users."www-data" = {
    isSystemUser = true;
    shell = pkgs.bashInteractive;
    group = "nginx";
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPBdjAblqgWEUSaMkVw+Bv6nmFpzYpsHpMYCquN3IARw www-data-deploy"
    ];
  };

  systemd.tmpfiles.rules = [ "d /srv/www 0775 nginx nginx -" ];

  services.acme-dns = {
    enable = true;
    settings = {
      api = {
        address = "[::1]";
        port = 8053;
      };
      general = {
        listen = "[2a01:4f8:c012:1a6a::]:53";
        domain = "acme-dns.muc.ccc.de";
        nsadmin = "noc.muc.ccc.de";
        nsname = "oaarchkatzl.muc.ccc.de";
        records = [
          "acme-dns.muc.ccc.de A 5.75.248.200"
          "acme-dns.muc.ccc.de AAAA 2a01:4f8:c012:1a6a::443"
          "acme-dns.muc.ccc.de CAA 0 issue letsencrypt.org"
          "acme-dns.muc.ccc.de NS oaarchkatzl.muc.ccc.de"
        ];
      };
    };
  };
}
