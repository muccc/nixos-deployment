{ pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];
  deployment.targetHost = "laborbox.club.muc.ccc.de";
  deployment.targetUser = "root";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "laborbox";

  time.timeZone = "Europe/Berlin";

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "a8:20:66:49:4b:bf";
    linkConfig.Name = "upl0nk";
  };

  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig.Bridge = "br-lan";
  };

  systemd.network.netdevs."30-br-lan".netdevConfig = {
    Kind = "bridge";
    Name = "br-lan";
  };

  systemd.network.networks."30-br-lan" = {
    matchConfig.Name = "br-lan";
    DHCP = "yes";
    networkConfig.IPv6PrivacyExtensions = "prefer-public";
  };

  # Enable NFS. This is used to share directories between the host OS and the
  # Windows VM.
  services.nfs.server = {
    enable = true;
    exports = ''
      /home/muccc/export 192.168.122.0/24(rw,no_subtree_check,sync,no_root_squash,all_squash,anonuid=1007,anongid=100)
    '';
    lockdPort = 4001;
    mountdPort = 4002;
    statdPort = 4000;
    extraNfsdConfig = '''';
  };

  services.xserver = {
    enable = true;
    desktopManager = {
      xterm.enable = false;
      xfce.enable = true;
    };
    displayManager.defaultSession = "xfce";
  };

  fileSystems."/" = {
    device = "/dev/disk/by-label/laborbox-root";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/laborboxesp";
    fsType = "vfat";
  };

  boot.kernelModules = [ "usbmon" ];

  services.udev.extraRules = ''
    SUBSYSTEM=="usbmon", OWNER="muccc"
  '';

  virtualisation.libvirtd.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6OymLFcMoKcNd58eo5aYQU6DIVY7sx4kWhyw4OWDPEKvRvYrOyFLLpNoHJpxSeTYw9jQWEvjvrFRW4zoFmTR+G495Szy7QCz4lmSoUUZReSiap2aRIJ3lyXsfrN9VgNhflg8FRTLBqkXdcNPwG6hnY9j45OMo+zqr4fMgQxLzvVzi/HDjT2Y43/iJCsKYCFcxsyflcGNbfpTztM7gPQGMZnEAxnlsiOaUgXTGlts+W7EWMFavtjHq9jm82OkugB3Z7ZbT5N3jCsuARtM4kWMFtZnMy/NBfcd7EaO4wxD8sMwiWaQGOkaqbE3wRXxZVr0RCK5TKwrRuNA/rYAGJ9cr ch3-muccc"
  ];
  users.users.muccc = {
    isNormalUser = true;
    uid = 1007; # needs to be configured in the windows VM as well
    extraGroups = [
      "wheel"
      "libvirtd"
    ];
    password = "fnord42";
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG599UildOrAq+LIOQjKqtGMwjgjIxozI1jtQQRKHtCP q3k@mimeomia"
    ];
  };

  environment.systemPackages = with pkgs; [
    vim
    wget
    curl
    htop
    dstat
    usbutils
    spice-gtk
    firefox
    virt-manager
    inkscape
    solvespace
    wireshark
  ];

  # Enables USB redirection for libvirt (I think)
  # https://github.com/NixOS/nixpkgs/issues/106594
  virtualisation.spiceUSBRedirection.enable = true;

  networking.firewall.allowedTCPPorts = [
    # NFS
    111
    2049
    4000
    4001
    4002
    20048
  ];
  networking.firewall.allowedUDPPorts = [
    # NFS
    111
    2049
    4000
    4001
    4002
    20048
  ];
  # Todo?
  networking.firewall.enable = false;

  system.stateVersion = "21.11";
}
