{
  inputs,
  config,
  pkgs,
  lib,
  ...
}:
let
  wifiIp4 = "192.168.2.2";
  sshCommandUsers = [
    {
      user = "open";
      msg = "Unlocking back door";
      cmd = "unlock";
    }
    {
      user = "openfront";
      msg = "Unlocking front door";
      cmd = "unlockfront";
    }
    {
      user = "close";
      msg = "Locking back door";
      cmd = "lock";
    }
    {
      user = "down";
      msg = "Setting state down";
      cmd = "down";
    }
    {
      user = "closed";
      msg = "Setting state closed";
      cmd = "closed";
    }
    {
      user = "member";
      msg = "Setting state member";
      cmd = "member";
    }
    {
      user = "public";
      msg = "Setting state public";
      cmd = "public";
    }
  ];
in
{
  system.stateVersion = "24.05";

  sops = {
    secrets.keyholders_deploy_token_env = { };
  };

  muccc.rpi.enable = config.muccc.targetDeploy;

  # draws less power, power supply needs more juice
  powerManagement.cpuFreqGovernor = "powersave";

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.route_localnet" = 1;
  };

  networking = {
    hostName = "luftschleuse";
    firewall = {
      interfaces.upl0nk.allowedTCPPorts = [ 22 ];
      interfaces.wlan0 = {
        allowedTCPPorts = [
          22
          53
        ];
        allowedUDPPorts = [
          53
          67
        ];
      };
      rejectPackets = true;
      extraCommands = ''
        iptables -t nat -A PREROUTING -p udp -s 192.168.2.0/24 --dport 53 -j DNAT --to-destination 192.168.2.2
        iptables -t nat -A PREROUTING -p tcp -s 192.168.2.0/24 --dport 80 -j DNAT --to-destination 192.168.2.2
        iptables -t nat -A PREROUTING -p tcp -s 192.168.2.0/24 --dport 443 -j DNAT --to-destination 192.168.2.2
      '';
      extraStopCommands = ''
        iptables -t nat -F PREROUTING
      '';
    };
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "b8:27:eb:02:53:2c";
    linkConfig = {
      Name = "upl0nk";
      MACAddress = "c4:93:00:11:0d:3d";
      MACAddressPolicy = "none";
    };
  };
  systemd.network.networks."40-upl0nk" = {
    matchConfig.Name = "upl0nk";
    linkConfig.RequiredForOnline = true;
    dhcpV4Config.ClientIdentifier = "mac";
    networkConfig.DHCP = "yes";
  };
  systemd.network.networks."40-wlan0" = {
    matchConfig.Name = "wlan0";
    linkConfig.RequiredForOnline = false;
    address = [ "${wifiIp4}/24" ];
    DHCP = "no";
  };

  environment.systemPackages = with pkgs; [ iw ];

  muccc.hardening.openssh.enable = false;

  services.openssh.extraConfig =
    let
      triggerScript = pkgs.writeScript "trigger.sh" ''
        #!${pkgs.stdenv.shell}
        set -o errexit
        case "$SSH_ORIGINAL_COMMAND" in
          unlock | unlockfront)
            echo "$SSH_ORIGINAL_COMMAND" | ${pkgs.netcat}/bin/nc -w 0 -u 127.0.0.1 2323
            echo "unlocking"
            ;;
          lock)
            echo "$SSH_ORIGINAL_COMMAND" | ${pkgs.netcat}/bin/nc -w 0 -u 127.0.0.1 2323
            echo "locking"
            ;;
          down | closed | member | public)
            echo "$SSH_ORIGINAL_COMMAND" | ${pkgs.netcat}/bin/nc -w 0 -u 127.0.0.1 2323
            echo "setting state $SSH_ORIGINAL_COMMAND"
            ;;
          *)
            echo "unknown command"
            ;;
        esac
      '';
    in
    ''
      Match User trigger
        LogLevel ERROR
        DisableForwarding yes
        AuthorizedKeysFile /etc/ssh/authorized_keys.d/%u /var/lib/authorized_keys/authorized_keys
        ForceCommand ${triggerScript}
    ''
    + lib.concatMapStrings (
      t:
      let
        commandScript = pkgs.writeScript "${t.user}.sh" ''
          #!${pkgs.stdenv.shell}
          set -o errexit
          echo '${t.msg}...'
          echo '${t.cmd}' | ${pkgs.netcat}/bin/nc -w 0 -u 127.0.0.1 2323
          echo 'Command sent.'
        '';
      in
      ''
        Match User ${t.user}
          LogLevel ERROR
          DisableForwarding yes
          AuthorizedKeysFile /etc/ssh/authorized_keys.d/%u /var/lib/authorized_keys/authorized_keys
          ForceCommand ${commandScript}
      ''
    ) sshCommandUsers;

  users = {
    mutableUsers = false;
    users =
      {
        root.openssh.authorizedKeys.keys = [
          # schneider
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCkQjXVf4bW5tqE9j3bh5buCVQeLVTJm3tpr0cvsPB5hxLjGhrCzD//7a4REwXp3QzWIzv2sKSl98IC6jOsD8zx1e8sHddgh3l+FltfLs+G7f1GOc5VjpKUlo+v5kZBdNyjbhzww/lQ7WKotvf/Kn1TpsuUdFjGfg384CfFKdr6/VuRybceLrzVMYcInM0mXYZViGvBn3Z66Oaaainc+DYN8v4cNpRbVj+IoCf+d03HReUbxqhl4t0Os9wTz9lC4uPXKzF+HVhixSOXVOd7qlWpHrZ2rK8JbOuCc2k8jAvOm5TfWIxhSUQAdlczETVTOhNvy1hyC67Al/sHvM9WHWJD"
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDe/Udd8U/ZBgGRfGT6nMYEs1aZ4NYjl55QeIrpHg3fb9xFHhJ6bFZyM6BVTeYroyvYRv1rXQLSYv7uzE7xa9WIznkjwyZUBH0WavWMlFds3YBER2UvFzJqu7989g1EWFTeEJM0tegkWFFwKtonVmnTdWVeWTdDWyIUlSLgpss/wxzStf22AmCXPH9AbQ3uWKRBqzSKeHq4mt9rcPXrhAbuY6g8/1yMBd2S25zm7jWFeQCXMOetniw2ugCF5TsUJEVBSxB4MGvUotUyugbEOzgIqOoBmUZ62x5FmqIZD9vcinbFlCX/grDtEbMQ60Re5LBU/95TVLiHH49fMdTo0jXF"
        ];
        trigger = {
          isNormalUser = true;
          group = "users";
          openssh.authorizedKeys = {
            inherit (config.users.users.root.openssh.authorizedKeys) keys;
          };
        };
      }
      // (lib.genAttrs
        [
          "open"
          "openfront"
          "close"
          "down"
          "closed"
          "member"
          "public"
        ]
        (_name: {
          isNormalUser = true;
          group = "users";
          openssh.authorizedKeys = {
            inherit (config.users.users.root.openssh.authorizedKeys) keys;
          };
        })
      );
  };

  systemd.services.dnsmasq =
    let
      units = [ "sys-subsystem-net-devices-wlan0.device" ];
    in
    {
      after = units;
      bindsTo = units;
      wantedBy = units;
    };
  services.dnsmasq = {
    enable = true;
    resolveLocalQueries = false;
    settings = {
      bind-interfaces = true;
      interface = "wlan0";
      leasefile-ro = true;
      dhcp-range = [ "192.168.2.10,192.168.2.200,30m" ];
      quiet-dhcp = true;
      no-hosts = true;
      no-resolv = true;
      address = [
        "/luftschleuse/${wifiIp4}"
        "/luftschleuse.club.muc.ccc.de/${wifiIp4}"
        "/connectivitycheck.gstatic.com/${wifiIp4}"
      ];
    };
  };

  systemd.services.hostapd = {
    wantedBy = config.systemd.services.hostapd.bindsTo;
  };

  services.hostapd = {
    enable = true;
    radios."wlan0" = {
      countryCode = "DE";
      channel = 1;
      wifi4 = {
        enable = true;
        capabilities = [
          "HT20"
          "SHORT-GI-20"
        ];
      };
      wifi5.enable = false;
      networks."wlan0" = {
        ssid = "luftschleuse";
        authentication = {
          mode = "wpa2-sha256";
          wpaPassword = "haileris";
        };
        # PMF not supported
        settings.ieee80211w = 0;
      };
    };
  };

  services.nginx = {
    enable = true;
    virtualHosts."luftschleuse.club.muc.ccc.de" = {
      default = true;
      locations."/generate_204".return = "204";
      locations."/".return = "200";
      extraConfig = ''
        allow 192.168.2.0/24;
        allow 10.189.0.0/16;
        allow 2a01:7e01:e003:8b00::/56;
        deny all;
      '';
    };
  };

  environment.etc."lockd.cfg".text = ''
    [Front Door]
    type = door
    address = A
    key = 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    inital_unlock = False
    sequence_number_container_file = /tmp/front_door_rx_seq.log
    rx_sequence_leap = 32768
    timeout = 2

    [Back Door]
    type = door
    address = B
    key = 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    inital_unlock = True
    sequence_number_container_file = /tmp/back_door_rx_seq.log
    rx_sequence_leap = 32768
    timeout = 2

    [Master Controller]
    serialdevice = /dev/null
    baudrate = 115200

    [Master Controller Buttons]
    down = 2
    closed = 1
    member = 4
    public = 8

    [Master Controller LEDs]
    down = 0
    closed = 1
    member = 2

    [Logging]
    host = 127.0.0.1
    port = 23514
    level = debug

    [Display]
    display_type = Nokia_1600
    max_update_rate = .5

    [Status Receiver]
    host = 127.0.0.1
    port = 2080
  '';

  systemd.services.lockd = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ hostapd ];
    serviceConfig = {
      Restart = "always";
      ExecStart = "${inputs.luftschleuse2.packages.${pkgs.system}.default}/bin/lockd /root/lockd.cfg";
    };
  };

  systemd.services.update-authorized-keys = {
    environment.PYTHONUNBUFFERED = "1";
    serviceConfig = {
      Type = "oneshot";
      ExecStart = lib.getExe (
        pkgs.writers.writePython3Bin "keyholder-deploy" {
          flakeIgnore = [
            "E501"
            "W503"
          ];
        } (builtins.readFile ../static/luftschleuse/keyholder-deploy.py)
      );
      StateDirectory = "authorized_keys";
      PrivateTmp = true;
      EnvironmentFile = [ config.sops.secrets.keyholders_deploy_token_env.path ];
      SuccessExitStatus = [
        "0"
        "42"
      ];
    };
  };

  systemd.timers.update-authorized-keys = {
    wantedBy = [ "timers.target" ];
    timerConfig.OnCalendar = "*:0/5";
  };
}
