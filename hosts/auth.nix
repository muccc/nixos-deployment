{ config, ... }:
{
  system.stateVersion = "23.05";

  sops = {
    secrets.authentik_secret = {
      restartUnits = [
        "authentik.service"
        "authentik-worker.service"
        "authentik-migrate.service"
      ];
    };
  };

  networking = {
    firewall.allowedTCPPorts = [ 9300 ];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "ba:12:0c:2c:be:aa";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = true;
    };
  };

  services.authentik = {
    enable = true;

    nginx = {
      enable = true;
      enableACME = true;
      host = "auth.muc.ccc.de";
    };

    environmentFile = config.sops.secrets.authentik_secret.path;
  };

  muccc.acme-dns.certs = {
    "auth.muc.ccc.de" = "acme-dns-auth";
    "auth.club.muc.ccc.de" = "acme-dns-auth";
  };

  services.nginx = {
    enable = true;
    virtualHosts."auth.muc.ccc.de" = {
      acmeRoot = null;
    };
    virtualHosts."auth.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/".return = "301 https://auth.muc.ccc.de$request_uri";
    };
  };
}
