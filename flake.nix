{
  description = "MuCCC Infrastructure Deployment";

  nixConfig = {
    extra-substituters = [
      "https://cache.club.muc.ccc.de"
      # "https://cache.muc.ccc.de/muccc"
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      # "muccc:ATRH5jlmJyn4kpMmnhMVIbEugyr8lsTYvi5KmadQvZk="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default-linux";

    # fixed legacy inputs for dependent flakes
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };
    flake-compat.url = "github:edolstra/flake-compat";

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };

    flake-root.url = "github:srid/flake-root";

    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.92.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
      };
    };

    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    docker-nixpkgs = {
      url = "github:fpletz/docker-nixpkgs";
      flake = false;
    };

    muccc-api = {
      url = "git+https://git.muc.ccc.de/muCCC/api";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-parts.follows = "flake-parts";
        systems.follows = "systems";
      };
    };

    flipdot-image-editor = {
      url = "github:lheckemann/SimplePixel/flipdot";
      flake = false;
    };

    luftschleuse2 = {
      url = "github:muccc/luftschleuse2/luftschleuse3";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-parts.follows = "flake-parts";
        systems.follows = "systems";
      };
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    mumble-exporter = {
      url = "github:mguentner/mumble_exporter";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    authentik = {
      url = "github:nix-community/authentik-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        flake-compat.follows = "flake-compat";
        flake-parts.follows = "flake-parts";
        systems.follows = "systems";
      };
    };

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    impermanence = {
      url = "github:nix-community/impermanence";
    };

    nix-fast-build = {
      url = "github:Mic92/nix-fast-build";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-parts.follows = "flake-parts";
        treefmt-nix.follows = "treefmt-nix";
      };
    };

    bad_gateway = {
      url = "github:mguentner/bad_gateway";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        systems.follows = "systems";
      };
    };

    zpl = {
      url = "github:FelixHenninger/zpl";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-parts.follows = "flake-parts";
        systems.follows = "systems";
      };
    };

    nnf = {
      url = "github:thelegy/nixos-nftables-firewall";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    nixos-passthru-cache = {
      url = "github:numtide/nixos-passthru-cache";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-anywhere = {
      url = "github:nix-community/nixos-anywhere";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixos-stable.follows = "nixpkgs";
        flake-parts.follows = "flake-parts";
        disko.follows = "disko";
        treefmt-nix.follows = "treefmt-nix";
      };
    };
  };

  outputs =
    inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;

      flake = {
        lib = {
          hosts = import ./lib/hosts.nix;
          humanoids = import ./lib/humanoids.nix;
        };
      };

      imports = [
        inputs.flake-root.flakeModule
        ./colmena.nix
        ./devshells.nix
        ./nixos.nix
        ./pkgs.nix
        ./treefmt.nix
      ];
    };
}
