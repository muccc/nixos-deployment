{ inputs, ... }:
{
  perSystem =
    {
      pkgs,
      system,
      lib,
      self',
      ...
    }:
    {
      _module.args.pkgs = import inputs.nixpkgs {
        inherit system;
        overlays = [
          inputs.lix-module.overlays.default
        ];
      };

      packages = {
        a2dp-agent = pkgs.callPackage ./pkgs/a2dp-agent.nix { };

        inherit (pkgs) attic-client skopeo;
        nix-fast-build = inputs.nix-fast-build.packages.${system}.default;

        sopsYaml = pkgs.callPackage ./pkgs/sops.yaml.nix { inherit (inputs) self; };
        repl = pkgs.writers.writeBashBin "repl" (lib.readFile ./static/nixos-repl.sh);

        sops-updatekeys = pkgs.writers.writeBashBin "sops-updatekeys" ''
          for secretfn in secrets/*.yaml; do
            ${pkgs.sops}/bin/sops --config=${self'.packages.sopsYaml} updatekeys $secretfn
          done
        '';

        bauwat = pkgs.writers.writeBashBin "bauwat" ''
          export NIX_CONFIG="builders = @$FLAKE_ROOT/static/machine-bauwagen"
          exec $@
        '';
      };

      legacyPackages = {
        ciNixosConfigurations = pkgs.callPackage ./pkgs/nixos-configurations.nix {
          inherit (self'.legacyPackages) colmenaNixosConfigurations;
        };

        prometheus-snmp-exporter-generator =
          pkgs.callPackage ./pkgs/prometheus-snmp-exporter-generator.nix
            { };
      };
    };
}
