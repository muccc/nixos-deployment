.
| {
  node_deployment_time_seconds: {
    labels: { nixosVersion, nixpkgsRevision, configurationRevision },
    value: $systemTimestamp | tonumber
  },
  node_nixpkgs_commit_time_seconds: {
    labels: { nixpkgsRevision },
    value: $nixpkgsTimestamp | tonumber
  },
  node_reboot_required: {
    labels: { },
    value: $rebootRequired | tonumber
  },
}
| to_entries
| map(
  (.value.labels | to_entries | map(.key + "=" + (.value | @json)) | join(",")) as $labels_text
  | "\(.key){\($labels_text)} \(.value.value)\n"
)
| add
