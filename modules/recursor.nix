{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.muccc.recursor;
in
{
  options = {
    muccc.recursor = {
      enable = lib.mkEnableOption "DNS recursor";

      listenIps = lib.mkOption {
        type = lib.types.listOf lib.types.str;
        default = [
          "[::1]"
          "127.0.0.1"
        ];
      };

      webmgmtListenIps = lib.mkOption {
        type = lib.types.listOf lib.types.str;
        default = [
          "::1"
          "127.0.0.1"
        ];
      };
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ knot-resolver ];

    users.users.knot-resolver.extraGroups = [ "acme" ];

    services.kresd = {
      enable = true;
      package = pkgs.knot-resolver.override (_: {
        extraFeatures = true;
      });
      instances = 4;
      listenPlain = map (ip: "${ip}:53") cfg.listenIps;
      listenTLS = map (ip: "${ip}:853") cfg.listenIps;
      listenDoH = map (ip: "${ip}:443") cfg.listenIps;
      extraConfig =
        let
          certDir = config.security.acme.certs."resolver.${config.networking.fqdn}".directory;
        in
        ''
          modules = { 'view', 'stats', 'predict', 'bogus_log', 'http', 'prefill', 'serve_stale < cache' }

          net.tls('${certDir}/cert.pem', '${certDir}/key.pem')
          http.prometheus.namespace = 'kresd_'

          policy.add(policy.suffix(policy.DENY, {todname('use-application-dns.net.')}))
          policy.add(
              policy.domains(
                  policy.ANSWER(
                      { [kres.type.SVCB] = { rdata=kres.parse_rdata({
                         'SVCB 1 resolver.transrapid.muc.ccc.de. alpn=dot,h2 ipv6hint=2a01:7e01:e003:8b00::53 ipv4hint=10.189.0.53 dohpath=/dns-query{?dns}',
                      }), ttl=5 } }
                  ), { todname('_dns.resolver.arpa') }))
          policy.add(policy.suffix(
            policy.STUB('::1@5553'),
            {todname('0.0.b.8.3.0.0.e.1.0.e.7.1.0.a.2.ip6.arpa')}))
          policy.add(policy.suffix(
            policy.STUB('::1@5553'),
            {todname('189.100.in-addr.arpa')}))
          policy.add(policy.suffix(
            policy.STUB('::1@5553'),
            {todname('access.club.muc.ccc.de')}))
          policy.add(policy.suffix(
            policy.STUB('::1@5553'),
            {todname('mgmt.club.muc.ccc.de')}))
          policy.add(policy.suffix(
            policy.STUB('::1@5553'),
            {todname('env.club.muc.ccc.de')}))
          policy.add(policy.suffix(
            policy.STUB('::1@5553'),
            {todname('iot.club.muc.ccc.de')}))
          policy.add(policy.suffix(
            policy.STUB('::1@5553'),
            {todname('cache.club.muc.ccc.de')}))

          prefill.config({
              ['.'] = {
                  url = 'https://www.internic.net/domain/root.zone',
                  interval = 86400,
              }
          })

          cache.size = 1024 * MB
        ''
        + lib.concatMapStringsSep "\n" (
          a: "net.listen('${a}', 8053, { kind = 'webmgmt' })"
        ) cfg.webmgmtListenIps;
    };
  };
}
