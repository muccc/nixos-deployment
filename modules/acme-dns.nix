{ config, lib, ... }:
let
  cfg = config.muccc.acme-dns;
in
{
  options = {
    muccc.acme-dns.certs = lib.mkOption {
      type = lib.types.attrsOf lib.types.str;
      default = { };
    };
  };

  config = {
    sops.secrets = lib.mapAttrs' (
      _domain: secret: lib.nameValuePair secret { owner = "acme"; }
    ) cfg.certs;

    sops.templates =
      (lib.mapAttrs' (
        domain: secret:
        lib.nameValuePair "acme-dns-${domain}-storage.json" {
          owner = "acme";
          content = ''{"${domain}":${config.sops.placeholder.${secret}}}'';
        }
      ) cfg.certs)
      // (lib.mapAttrs' (
        domain: _secret:
        lib.nameValuePair "acme-dns-${domain}" {
          owner = "acme";
          content = ''
            ACME_DNS_API_BASE="https://acme-dns.muc.ccc.de"
            ACME_DNS_STORAGE_PATH="${config.sops.templates."acme-dns-${domain}-storage.json".path}"
          '';
        }
      ) cfg.certs);

    security.acme.certs = lib.mapAttrs (domain: _secret: {
      dnsProvider = "acme-dns";
      dnsPropagationCheck = false;
      environmentFile = config.sops.templates."acme-dns-${domain}".path;
    }) cfg.certs;
  };
}
