{ lib, config, ... }:
let
  cfg = config.muccc.qemu-guest;
in
{
  options = {
    muccc.qemu-guest = {
      enable = lib.mkOption {
        type = lib.types.bool;
        default = true;
        description = "qemu guest profile";
      };

      uefi = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = "UEFI boot";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    boot = {
      initrd = {
        availableKernelModules = [
          "virtio_net"
          "virtio_pci"
          "virtio_mmio"
          "virtio_blk"
          "virtio_scsi"
          "9p"
          "9pnet_virtio"
        ];
        kernelModules = [
          "virtio_balloon"
          "virtio_console"
          "virtio_rng"
        ];
      };
      kernelParams = [ "console=ttyS0" ];
      loader = {
        timeout = lib.mkDefault 1;
        grub = {
          enable = lib.mkDefault (!cfg.uefi);
          device = "/dev/vda";
        };
        systemd-boot.enable = cfg.uefi;
      };
    };

    fileSystems = {
      "/" = lib.mkDefault {
        device = "/dev/disk/by-label/nixos";
        fsType = "ext4";
        autoResize = true;
      };
    };

    services.qemuGuest.enable = true;

    disko.devices = lib.mkIf cfg.uefi {
      disk = {
        vda = {
          device = "/dev/vda";
          type = "disk";
          content = {
            type = "gpt";
            partitions = {
              ESP = {
                type = "EF00";
                size = "200M";
                content = {
                  type = "filesystem";
                  format = "vfat";
                  mountpoint = "/boot";
                  mountOptions = [ "umask=0077" ];
                };
              };
              root = {
                size = "100%";
                content = {
                  type = "filesystem";
                  format = "ext4";
                  mountpoint = "/";
                  mountOptions = [ "noatime" ];
                };
              };
            };
          };
        };
      };
    };
  };
}
