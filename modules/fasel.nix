{ lib, config, ... }:

let
  cfg = config.muccc.fasel;
in
{
  options.muccc.fasel = {
    enable = lib.mkEnableOption "Mumble Fasel";
  };

  config = lib.mkIf cfg.enable {
    networking = {
      firewall.allowedTCPPorts = [ config.services.murmur.port ];
      firewall.allowedUDPPorts = [ config.services.murmur.port ];
    };

    services.nginx = {
      enable = true;
      virtualHosts."fasel.muc.ccc.de" = {
        enableACME = true;
        forceSSL = true;
        locations."/".return = "200 ''";
      };
    };

    services.murmur = {
      enable = true;
      registerName = "Club Mumble";
      allowHtml = true;
      logDays = -1;
      logFile = null;
      password = "";
      registerHostname = "fasel.muc.ccc.de";
      welcometext = "Willkommen im µc³ Mumble.";
      sslCert = "/var/lib/acme/fasel.muc.ccc.de/fullchain.pem";
      sslKey = "/var/lib/acme/fasel.muc.ccc.de/key.pem";
      bandwidth = 130000;
      extraConfig = ''
        ice="tcp -h 127.0.0.1 -p 6502"
        autobanAttempts=0
        obfuscate=true
        opusthreshold=0
        suggestPushToTalk=true
      '';
    };

    users.extraGroups.nginx.members = [ "murmur" ];

    security.acme.certs = {
      "fasel.muc.ccc.de" = {
        postRun = ''
          systemctl restart murmur
        '';
      };
    };

  };
}
