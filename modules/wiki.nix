{
  lib,
  pkgs,
  config,
  ...
}:
let
  cfg = config.muccc.wiki;
in
{
  options.muccc.wiki = {
    enable = lib.mkEnableOption "muc.ccc.de";
  };

  config = lib.mkIf cfg.enable {
    users.users.wiki = {
      isSystemUser = true;
      group = "wiki";
      home = "/var/lib/wiki";
    };
    users.groups.wiki = { };

    services.phpfpm.pools."wiki" = {
      phpPackage = pkgs.php84;
      user = "wiki";
      group = "wiki";
      settings = {
        "listen.owner" = "nginx";
        "listen.group" = "nginx";
        "listen.mode" = "0600";
        "pm" = "dynamic";
        "pm.max_children" = 32;
        "pm.start_servers" = 2;
        "pm.min_spare_servers" = 2;
        "pm.max_spare_servers" = 4;
        "pm.max_requests" = 500;
      };
      phpOptions = ''
        catch_workers_output = On
        short_open_tag = Off
        open_basedir = 
        output_buffering = Off
        output_handler = Off
        zlib.output_compression = Off
        implicit_flush = Off
        allow_call_time_pass_reference = Off
        max_execution_time = 30
        max_input_time = 60
        max_input_vars = 10000
        memory_limit = 128M
        error_reporting = E_ALL & ~E_NOTICE
        display_errors = Off
        display_startup_errors = Off
        log_errors = On
        variables_order = "EGPCS"
        register_argc_argv = On
        file_uploads = On
        session.use_cookies = 1
      '';
    };

    services.nginx = {
      enable = true;
      virtualHosts = {
        "muc.ccc.de" = {
          serverAliases = [
            "wiki.muc.ccc.de"
            "eh2010.muc.ccc.de"
            "schule.muc.ccc.de"
            "blog.muc.ccc.de"
            "radio.muc.ccc.de"
            "acab2010.muc.ccc.de"
            "asm18.muc.ccc.de"
            "asm17.muc.ccc.de"
            "asm16.muc.ccc.de"
            "sdr-challenge.muc.ccc.de"
            "www.muc.ccc.de"
            "muenchen.ccc.de"
            "wiki.muenchen.ccc.de"
          ];
          forceSSL = true;
          enableACME = true;
          root = "/var/lib/wiki/dokuwiki";
          # locations = {
          #   "~ ^/.*\\.php$".extraConfig = ''
          #     fastcgi_pass  unix:${config.services.phpfpm.pools.wiki.socket};
          #   '';
          # };
          extraConfig = ''
            index doku.php;

            # add_header X-Content-Type-Options nosniff;
            # add_header X-XSS-Protection "1; mode=block";
            # #add_header X-Robots-Tag none;
            # add_header X-Download-Options noopen;
            # add_header X-Permitted-Cross-Domain-Policies none;
            # add_header 'Referrer-Policy' 'no-referrer';

            client_max_body_size 23M;
            client_body_buffer_size 128k;

            location ~ ^/intern:plenum-([^\r\n]*)$ {
              return 302 /intern:plenum:$1;
            }

            location ~ ^/dokuwiki/data/ {
              internal;
              root /public;
            }

            location ~^/\.ht { deny all; } # also secure the Apache .htaccess files
            location ~^/(data|conf|bin|inc)/ { deny all; } # also secure the Apache .htaccess files

            location @dokuwiki {
              #rewrites "doku.php/" out of the URLs if you set the userewrite setting to .htaccess in dokuwiki config page
              rewrite ^/_media/(.*) /lib/exe/fetch.php?media=$1 last;
              rewrite ^/_detail/(.*) /lib/exe/detail.php?media=$1 last;
              rewrite ^/_export/([^/]+)/(.*) /doku.php?do=export_$1&id=$2 last;

              rewrite ^/_img/((.*)/)?(.*)        /lib/exe/fetch.php?mode=styleimg&media=$3&template=$2 last;
              rewrite ^/images/((.*)/)?(.*)      /lib/exe/fetch.php?mode=styleimg&media=$3&template=$2 last;
              rewrite ^/lib/tpl/((.*)/)?images/(.*) /lib/exe/fetch.php?mode=styleimg&media=$3&template=$2 last;

              rewrite ^/(.*) /doku.php?id=$1&$args last;
            }

            # Redirect for the specific subdomain and path
            location /cryptoparty {
              if ($host = muc.ccc.de) {
                return 301 https://wiki.muc.ccc.de/cryptoparty:start;
              }
              try_files $uri $uri/ @dokuwiki;
            }

            location /.well-known/matrix/client {
              default_type application/json;
              if ($host = muc.ccc.de) {
                return 200 '{ "m.homeserver": { "base_url": "https://matrix.muc.ccc.de" }, "im.vector.riot.jitsi": { "preferredDomain": "webex.muc.ccc.de" } }';
                add_header Access-Control-Allow-Origin *;
              }
              return 404 '{}';
            }
            location /.well-known/matrix/server {
              default_type application/json;
              if ($host = muc.ccc.de) {
                return 200 '{"m.server": "matrix.muc.ccc.de"}';
              }
              return 404 '{}';
            }

            location / { try_files $uri $uri/ @dokuwiki; }

            location ~ \.php$ {
              try_files $uri =404;
              fastcgi_pass unix:${config.services.phpfpm.pools.wiki.socket};
              # fastcgi_param PATH_INFO $fastcgi_path_info;
              # fastcgi_split_path_info ^(.+\.php)(/.+)$;
              fastcgi_index index.php;
              include ${config.services.nginx.package}/conf/fastcgi.conf;
              fastcgi_param  REMOTE_USER        $remote_user;
            }
          '';
        };
      };
    };
  };
}
