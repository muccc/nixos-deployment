{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.muccc.git;
in
{
  options.muccc.git = {
    enable = lib.mkEnableOption "muccc git";
    domain = lib.mkOption {
      type = lib.types.str;
      default = "git.muc.ccc.de";
    };
  };

  config = lib.mkIf cfg.enable {
    sops = {
      secrets.smtp_auth = {
        owner = config.services.forgejo.user;
      };
    };

    users.users.git = {
      home = config.services.forgejo.stateDir;
      useDefaultShell = true;
      group = config.services.forgejo.group;
      isSystemUser = true;
    };
    users.groups.${config.services.forgejo.group} = { };

    services.forgejo = {
      enable = true;
      package = pkgs.forgejo;
      user = "git";
      group = "git";
      database = {
        type = "postgres";
        name = "git";
        user = "git";
      };
      lfs.enable = true;
      settings = {
        DEFAULT = {
          APP_NAME = "muCCC git";
        };
        server = {
          ROOT_URL = "https://${cfg.domain}/";
          HTTP_ADDR = "::1";
          HTTP_PORT = 3022;
          DOMAIN = "${cfg.domain}";
          LANDING_PAGE = "explore";
        };
        security = {
          LOGIN_REMEMBER_DAYS = 365;
        };
        repository = {
          DISABLE_STARS = true;
        };
        service = {
          DISABLE_REGISTRATION = true;
          SHOW_MILESTONES_DASHBOARD_PAGE = false;
          ENABLE_NOTIFY_MAIL = true;
          ENABLE_TIMETRACKING = false;
        };
        "service.explore" = {
          DISABLE_USERS_PAGE = true;
        };
        session = {
          PROVIDER = "redis";
          PROVIDER_CONFIG = "network=unix,addr=/run/redis-forgejo/redis.sock,db=0";
          COOKIE_SECURE = true;
          SESSION_LIFE_TIME = 7 * 24 * 60 * 60;
        };
        ui = {
          SHOW_USER_EMAIL = false;
        };
        "ui.meta" = {
          AUTHOR = "muCCC";
          DESCRIPTION = "muCCC git";
          KEYWORDS = "muccc,ccc,muc,git,forge,forgejo";
        };
        metrics = {
          ENABLED = true;
        };
        actions = {
          ENABLED = true;
        };
        other = {
          SHOW_FOOTER_VERSION = false;
          SHOW_FOOTER_TEMPLATE_LOAD_TIME = false;
        };
        oauth2_client = {
          ENABLE_OPENID_SIGNUP = true;
          ENABLE_AUTO_REGISTRATION = true;
          REGISTER_EMAIL_CONFIRM = false;
          OPENID_CONNECT_SCOPES = "email profile git";
        };
        mailer = {
          ENABLED = true;
          PROTOCOL = "smtp+starttls";
          SMTP_ADDR = "briafzentrum.muc.ccc.de";
          SMTP_PORT = 25;
          USER = "git";
          PASSWD = config.sops.secrets.smtp_auth.path;
          FROM = "git@muc.ccc.de";
        };
        cache = {
          ENABLED = true;
          ADAPTER = "redis";
          HOST = "network=unix,addr=/run/redis-forgejo/redis.sock,db=0";
        };
        indexer = {
          REPO_INDEXER_ENABLED = true;
        };
        "git.config" = {
          "diff.algorithm" = "histogram";
        };
        "git.timeout" = {
          DEFAULT = 360;
          MIGRATE = 900;
          MIRROR = 600;
          CLONE = 600;
          PULL = 600;
          GC = 360;
        };
        packages.CHUNKED_UPLOAD_PATH = "${config.services.forgejo.stateDir}/tmp/package-upload";
      };
    };

    services.redis.servers.forgejo = {
      enable = true;
      inherit (config.services.forgejo) user;
    };

    services.prometheus.exporters.redis = {
      enable = true;
      openFirewall = true;
    };

    services.nginx = {
      enable = true;
      virtualHosts.${cfg.domain} = {
        enableACME = true;
        forceSSL = true;
        locations = {
          "/" = {
            proxyPass = "http://[::1]:3022";
            extraConfig = ''
              client_max_body_size 2G;
            '';
          };
          "=/robots.txt".root = toString ../static/git;
        };
      };
      # legacy domain
      virtualHosts."gitea.muc.ccc.de" = {
        enableACME = true;
        forceSSL = true;
        locations."/".return = "301 https://${cfg.domain}$request_uri";
      };
    };

  };
}
