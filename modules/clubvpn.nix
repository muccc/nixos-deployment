{ lib, config, ... }:

let
  cfg = config.muccc.clubvpn;
in
{
  options.muccc.clubvpn = {
    enable = lib.mkEnableOption "VPN to the club for members";
    hostName = lib.mkOption {
      type = lib.types.str;
      default = "${config.networking.hostName}.${config.networking.domain}";
    };
    cidr = lib.mkOption {
      type = lib.types.str;
    };
    cidrv6 = lib.mkOption {
      type = lib.types.str;
    };
  };

  config = lib.mkIf cfg.enable {
    sops.secrets.wg-access-server-clubvpn = {
      restartUnits = [ "wg-access-server.service" ];
    };

    networking.firewall.allowedUDPPorts = [
      config.services.wg-access-server.settings.wireguard.port
    ];

    services.nginx = {
      enable = true;
      virtualHosts."clubvpn.${cfg.hostName}" = {
        enableACME = true;
        forceSSL = true;
        locations."/".proxyPass = "http://[::1]:${toString config.services.wg-access-server.settings.port}";
      };
    };

    services.wg-access-server = {
      enable = true;
      settings = {
        adminUsername = "wgadmin";
        externalHost = cfg.hostName;
        port = 51880;
        filename = "wg-muccc";
        storage = "sqlite3://db.sqlite";
        wireguard = {
          interface = "clubvpn";
          port = 51820;
          mtu = 1300;
        };
        vpn = {
          inherit (cfg) cidr cidrv6;
          allowedIPs = [
            "2a01:7e01:e003:8b00::/56"
            "10.189.0.0/16"
          ];
          nat44 = false;
          nat66 = false;
        };
        dns = {
          enabled = true;
          upstream = [ "2a01:7e01:e003:8b00::53" ];
          domain = "clubvpn.${config.networking.hostName}.muc.ccc.de";
        };
        clientconfig = {
          dnsServers = [ "2a01:7e01:e003:8b00::53" ];
          dnsSearchDomain = "club.muc.ccc.de";
          mtu = 1300;
        };
        auth = {
          oidc = {
            name = "auth.muc.ccc.de";
            issuer = "https://auth.muc.ccc.de/application/o/clubvpn-${config.networking.hostName}/";
            redirectURL = "https://clubvpn.${cfg.hostName}/callback";
            scopes = [
              "openid"
              "profile"
              "wg_access"
            ];
            claimMapping = {
              admin = "wg_access == 'admin'";
              access = "wg_access == 'access'";
            };
            claimsFromIDToken = true;
          };
        };
      };
      secretsFile = config.sops.secrets.wg-access-server-clubvpn.path;
    };
  };
}
