{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:
let
  cfg = config.muccc.snmpExporter;
  inherit (lib) mkEnableOption mkOption types;
in
{
  options.muccc.snmpExporter = {
    enable = mkEnableOption (lib.mdDoc "Muccc SNMP exporter");
    extraMIBs = mkOption {
      type = types.listOf types.path;
      description = lib.mdDoc "List of directories to be added to the MIB search path.";
      default = [ ];
    };
    librenmsSubdirs = mkOption {
      type = types.listOf types.str;
      description = lib.mdDoc "List of librenms subdirs to be added to the MIB search path.";
      default = [ ];
    };
    config = mkOption {
      type = (pkgs.formats.json { }).type;
      description = "Configuration for the SNMP exporter";
    };
    targets = mkOption {
      default = { };
      type = types.attrsOf (
        types.submodule {
          options = {
            targets = lib.mkOption { type = types.listOf types.str; };
            module = lib.mkOption { type = types.str; };
            auth = lib.mkOption { type = types.str; };
            interval = lib.mkOption { type = types.str; };
          };
        }
      );
    };
  };
  config = lib.mkIf cfg.enable {
    services.prometheus.exporters.snmp = {
      enable = true;
      configurationPath = inputs.self.legacyPackages.${pkgs.system}.prometheus-snmp-exporter-generator {
        inherit (cfg) librenmsSubdirs extraMIBs config;
      };
    };
  };
}
