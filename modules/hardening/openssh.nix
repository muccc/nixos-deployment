{ lib, config, ... }:
let
  cfg = config.muccc.hardening.openssh;
in
{
  options = {
    muccc.hardening.openssh = {
      enable = lib.mkEnableOption {
        default = true;
        description = "hardended ssh configuration";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    services.openssh = {
      settings = {
        PasswordAuthentication = false;
        Ciphers = [ "chacha20-poly1305@openssh.com" ];
        KexAlgorithms = [ "curve25519-sha256@libssh.org" ];
        Macs = [
          "hmac-sha2-512-etm@openssh.com"
          "hmac-sha2-256-etm@openssh.com"
          "umac-128-etm@openssh.com"
        ];
      };
      moduliFile = ../static/ssh-moduli;
    };
  };
}
