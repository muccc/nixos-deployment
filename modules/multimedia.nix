{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.muccc.multimedia;
in
{
  options = {
    muccc.multimedia = {
      enable = lib.mkEnableOption "multimediale unterhaltung";
      fqdn = lib.mkOption { type = lib.types.str; };
      username = lib.mkOption { type = lib.types.str; };
      greetdLoginCmd = lib.mkOption {
        type = lib.types.str;
        default = "sway";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    muccc.multimedia.fqdn = lib.mkOptionDefault "${config.networking.hostName}.${config.networking.domain}";

    networking.firewall = {
      enable = true;
      allowedTCPPorts = [
        6680 # mpd
        4711 # pipewire
        4713 # pulseaudio
        # mediamtx
        8554
        8322
        8000
        8001
        1935
        1936
        8888
        8889
      ];
      allowedUDPPorts = [
        # mediamtx
        8554
        8322
        8000
        8001
        8890
        8189
      ];
    };

    services.openssh.settings.PasswordAuthentication = true;

    xdg = {
      icons.enable = true;
      mime.enable = true;
    };

    hardware.bluetooth = {
      enable = true;
      powerOnBoot = true;
      settings = {
        General = {
          MultiProfile = "multiple";
          FastConnectable = true;
        };
      };
    };

    services.nginx = {
      enable = true;
      virtualHosts."${cfg.fqdn}" = {
        forceSSL = true;
        enableACME = true;
        acmeRoot = null;
        locations."/".proxyPass = "http://localhost:8081";
      };
    };

    services.mympd = {
      enable = true;
      settings = {
        http_port = 8081;
      };
    };
    users = {
      users.${cfg.username} = {
        isNormalUser = true;
        password = "alarm";
        extraGroups = [
          "wheel"
          "audio"
          "video"
          "dialout"
        ];
        linger = true;
        openssh.authorizedKeys.keys =
          config.users.users.root.openssh.authorizedKeys.keys
          ++ inputs.self.lib.humanoids.getSSHKeysForUsers [
            "fpletz"
            "derandere"
            "tuedel"
            "sebtm"
          ];
      };
    };

    home-manager.users.${cfg.username} =
      { pkgs, ... }:
      {
        home.stateVersion = "24.11";
        systemd.user.startServices = "sd-switch";

        nix = {
          gc = {
            automatic = true;
            options = "-d";
            frequency = "daily";
          };
        };

        gtk = {
          enable = true;
          theme = {
            name = "Arc-Dark";
            package = pkgs.arc-theme;
          };
          iconTheme = {
            name = "Papirus-Dark";
            package = pkgs.papirus-icon-theme;
          };
          gtk3.extraConfig = {
            gtk-application-prefer-dark-theme = true;
          };
          gtk4.extraConfig = {
            gtk-application-prefer-dark-theme = true;
          };
        };

        qt = {
          enable = true;
          platformTheme.name = "gtk";
        };

        programs.bash.enable = true;

        programs.tmux = {
          enable = true;
          aggressiveResize = true;
          clock24 = true;
          terminal = "tmux-256color";
          plugins = [
            {
              plugin = pkgs.tmuxPlugins.resurrect.overrideAttrs (_: {
                preFixup = ''
                  rm -r $out/share/tmux-plugins/resurrect/{run_,}tests
                '';
              });
              extraConfig = ''
                set -g @resurrect-processes 'ssh cmatrix "~mpv->mpv *" "~bash->bash *" "~vis->vis" "~btop->btop" htop "~python3->python3 *" mmtc bluetoothctl gomatrix "~unimatrix->unimatrix *" wttr'
                set -g @resurrect-dir '~/.local/state/tmux/resurrect'
              '';
            }
            {
              plugin = pkgs.tmuxPlugins.continuum;
              extraConfig = ''
                set -g @continuum-restore 'on'
                set -g @continuum-save-interval '1' # minutes
              '';
            }
          ];
        };

        programs.foot = {
          enable = true;
          server.enable = true;
          settings = {
            main = {
              font = "Fira Code:size=12";
            };
            colors = {
              alpha = 0.95;
            };
          };
        };

        services.mpd = {
          enable = true;
          musicDirectory = "/home/${cfg.username}";
          network.listenAddress = "any";
          extraConfig = ''
            audio_output {
              type "pipewire"
              name "pipewire"
            }
            zeroconf_enabled "yes"
            zeroconf_name "mpd @ %h"
          '';
        };

        systemd.user.services.mpd = {
          Unit.BindsTo = [ "pipewire.service" ];
          Service.ExecStartPost = "${lib.getExe pkgs.mpc-cli} play";
        };

        wayland.windowManager.sway = {
          enable = true;
          wrapperFeatures.gtk = true;
          config = {
            modifier = "Mod4";
            bars = [ ];
            terminal = "foot";
            window = {
              titlebar = false;
              hideEdgeBorders = "smart";
            };
            floating = {
              titlebar = false;
            };
            startup = [ { command = "${pkgs.foot}/bin/foot tmux attach"; } ];
            output = {
              HDMI-A-1.bg = "${./../static/multimedia/wau.jpg} fill";
              HEADLESS-1 = {
                mode = "1920x1080@60Hz";
              };
            };
          };
        };

        programs.waybar = {
          enable = true;
          systemd.enable = true;
        };

        programs.mpv = {
          enable = true;
          config = {
            vo = "gpu-next";
            gpu-context = "wayland";
            ytdl-format = lib.concatStringsSep "/" [
              "bestvideo[[vcodec~='^av01.*']][height<=?720]+bestaudio[acodec=opus]"
              "bestvideo[[vcodec~='^vp09.*']][height<=?720]+bestaudio[acodec=opus]"
              "bestvideo[height<=?720]+bestaudio[acodec=opus]"
              "bestvideo[height<=?720]+bestaudio"
              "best"
            ];
          };
        };

        home.packages = with pkgs; [
          yt-dlp
          pulsemixer
          cmatrix
          screen
          cli-visualizer
          playerctl
          mpc-cli
          mmtc
          btop
          gomatrix
          unimatrix
          (writers.writeBashBin "wttr" ''
            while true; do
              clear
              ${lib.getExe curl} -s wttr.in/~muccc?1 | head -n17 | tail -n10 && sleep 175
              sleep 5
            done
          '')
        ];

        services.spotifyd = {
          enable = true;
          package = pkgs.spotifyd.override {
            withALSA = false;
            withPortAudio = false;
            withMpris = true;
          };
          settings.global = {
            device_name = config.networking.hostName;
            backend = "pulseaudio";
            bitrate = 320;
            device_type = "computer";
          };
        };

        programs.librewolf = {
          enable = true;
          package = pkgs.librewolf-wayland.override (_attrs: {
            extraPrefs = ''
              pref("ui.systemUsesDarkTheme", 1);
              pref("privacy.webrtc.legacyGlobalIndicator", false);
              pref("media.ffmpeg.vaapi.enabled", true);
              pref("gfx.webrender.enabled", true);
              pref("gfx.webrender.all", true);
              pref("gfx.webrender.compositor", true);
              pref("gfx.webrender.compositor.force-enabled", true);
              pref("webgl.disabled", false);
              pref("privacy.resistFingerprinting", false);
              pref("network.dns.echconfig.enabled", true);
              pref("network.dns.http3_echconfig.enabled", true);
              pref("geo.enabled", false);
            '';
          });
        };

        programs.chromium = {
          enable = true;
          package = pkgs.ungoogled-chromium;
          commandLineArgs = [
            "--enable-features=WebRTCPipeWireCapturer,VaapiVideoDecoder"
            "--ignore-gpu-blocklist"
            "--enable-gpu-rasterization"
            "--disable-background-networking"
            "--enable-accelerated-video-decode"
            "--enable-zero-copy"
            "--no-default-browser-check"
            "--disable-sync"
            "--disable-features=MediaRouter"
            "--enable-features=UseOzonePlatform"
          ];
          extensions = [
            "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
            "pgdnlhfefecpicbbihgmbmffkjpaplco" # ublock extra
          ];
        };

        systemd.user.services.shairport-sync = {
          Unit = {
            After = [
              "network.target"
              "sound.target"
              "avahi-daemon.service"
            ];
            Description = "Shairport-Sync - Apple Airplay";
          };

          Install.WantedBy = [ "default.target" ];

          Service = {
            ExecStart = "${pkgs.shairport-sync}/bin/shairport-sync -v -o pw -a \"${config.networking.hostName}\"";
            RuntimeDirectory = "shairport-sync";
            Restart = "always";
          };
        };

        systemd.user.services.bt-agent-a2dp = {
          Unit = {
            Description = "A2DP Bluetooth Agent";
          };
          Install.WantedBy = [ "default.target" ];
          Service = {
            ExecStartPre = "${pkgs.bluez}/bin/bluetoothctl discoverable on";
            ExecStart = lib.getExe inputs.self.packages.${pkgs.system}.a2dp-agent;
            Environment = "PYTHONUNBUFFERED=1";
          };
        };
      };

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
      extraConfig = {
        pipewire."networking" = {
          "context.modules" = [
            {
              name = "libpipewire-module-protocol-simple";
              args = {
                playback = true;
                "audio.rate" = 48000;
                "audio.format" = "S16";
                "audio.channels" = 2;
                "audio.position" = [
                  "FL"
                  "FR"
                ];
                "server.address" = [
                  "tcp:4711"
                ];
              };
            }
            {
              name = "libpipewire-module-rtp-sap";
              args = {
                "local.ifname" = "upl0nk";
                "stream.rules" = [
                  {
                    matches = [
                      {
                        "sess.sap.announce" = true;
                      }
                    ];
                    actions = {
                      announce-stream = { };
                    };
                  }
                  {
                    matches = [
                      {
                        "rtp.session" = "~^${config.networking.hostName}.*";
                      }
                    ];
                    actions = {
                      create-stream = {
                        "sess.latency.msec" = 250;
                      };
                    };
                  }
                ];
              };
            }
          ];
        };
        pipewire-pulse."networking" = {
          "pulse.cmd" = [
            {
              cmd = "load-module";
              args = "module-zeroconf-publish";
            }
            {
              cmd = "load-module";
              args = "module-zeroconf-discover";
            }
          ];
          "pulse.properties" = {
            "server.address" = [
              "unix:native"
              {
                address = "tcp:4713";
                max-clients = 23;
                listen-backlog = 8;
                "client.access" = "restricted";
              }
            ];
          };
        };
      };
    };
    security.rtkit.enable = true;

    programs.sway.enable = true;
    fonts.fontconfig.enable = true;
    fonts.packages = with pkgs; [
      fira
      fira-code
      fira-code-symbols
      fira-mono
      powerline-fonts
      font-awesome_4
      font-awesome_5
    ];

    services.greetd = {
      enable = true;
      settings = {
        initial_session = {
          command = cfg.greetdLoginCmd;
          user = cfg.username;
        };
        default_session = {
          command = "${
            lib.makeBinPath [ pkgs.greetd.tuigreet ]
          }/tuigreet --time --cmd '${cfg.greetdLoginCmd}'";
          user = "greeter";
        };
      };
    };

    services.resolved = {
      llmnr = "false"; # we use avahi instead
    };
    services.avahi = {
      enable = true;
      publish = {
        enable = true;
        addresses = true;
        domain = true;
        hinfo = true;
        userServices = true;
      };
    };

    muccc.acme-dns.certs = {
      ${cfg.fqdn} = "acme-dns-multimedia";
    };
    sops.secrets = {
      acme-dns-multimedia.sopsFile = ../secrets/multimedia.yaml;
    };

    # access to tls certificates
    systemd.services.mediamtx.serviceConfig.SupplementaryGroups = [ "nginx" ];

    services.mediamtx = {
      enable = true;
      allowVideoAccess = false;
      settings =
        {
          writeQueueSize = 8192;
          api = true;
          apiAddress = "[::1]:9997";
          metrics = true;
          metricsAddress = "[::1]:9998";

          rtsp = true;
          rtspAddress = "[::]:8554";
          rtspsAddress = "[::]:8322";
          rtpAddress = "[::]:8000";
          rtcpAddress = "[::]:8001";
          protocols = [
            "udp"
            "tcp"
          ];

          rtmp = true;
          rtmpAddress = "[::]:1935";
          rtmpsAddress = "[::]:1936";

          hls = true;
          hlsAddress = "[::]:8888";
          hlsAlwaysRemux = true;
          hlsVariant = "lowLatency";
          hlsSegmentCount = 15;
          hlsSegmentDuration = "1s";
          hlsPartDuration = "200ms";
          hlsAllowOrigin = "*";
          hlsTrustedProxies = [
            "::1"
            "127.0.0.1"
          ];

          webrtc = true;
          webrtcAddress = "[::]:8889";
          webrtcAllowOrigin = "*";
          webrtcTrustedProxies = [ ];
          webrtcLocalUDPAddress = ":8189";
          webrtcICEServers2 = [ ];
          webrtcIPsFromInterfaces = true;
          webrtcIPsFromInterfacesList = [ "upl0nk" ];

          srt = true;
          srtAddress = "[::]:8890";

          paths.all_others.source = "publisher";
        }
        // (
          let
            serverCert = "/var/lib/acme/${cfg.fqdn}/full.pem";
            serverKey = "/var/lib/acme/${cfg.fqdn}/key.pem";
          in
          {
            inherit serverCert serverKey;
            rtmpEncryption = "optional";
            rtmpServerKey = serverKey;
            rtmpServerCert = serverCert;
            hlsEncryption = true;
            hlsServerKey = serverKey;
            hlsServerCert = serverCert;
            webrtcEncryption = true;
            webrtcServerKey = serverKey;
            webrtcServerCert = serverCert;
          }
        );
    };
  };
}
