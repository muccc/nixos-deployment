{ lib, config, ... }:
let
  cfg = config.muccc.hcloud.aarch64;
in
{
  options = {
    muccc.hcloud.aarch64.enable = lib.mkEnableOption "Hetzner Cloud AArch64";
  };

  config = lib.mkIf (cfg.enable && config.muccc.targetDeploy) {
    nixpkgs.system = "aarch64-linux";

    boot.kernelParams = lib.mkAfter [ "console=tty" ];
    boot.initrd = {
      kernelModules = [ "virtio_gpu" ];
      availableKernelModules = [
        "virtio_pci"
        "xhci_pci"
        "virtio_scsi"
        "usbhid"
        "sr_mod"
      ];
    };

    muccc.qemu-guest.enable = true;

    boot.loader = {
      grub.enable = false;

      systemd-boot = {
        enable = lib.mkForce true;
        consoleMode = "max";
      };
      efi.canTouchEfiVariables = true;
    };

    services.cloud-init = {
      enable = lib.mkDefault true;
      network.enable = true;
    };
  };
}
