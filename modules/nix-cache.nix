{
  lib,
  config,
  inputs,
  ...
}:
let
  cfg = config.muccc.nix-cache;
in
{
  imports = [
    # inputs.attic.nixosModules.atticd
    inputs.nixos-passthru-cache.nixosModules.nixos-passthru-cache
  ];

  options.muccc.nix-cache = {
    enable = lib.mkEnableOption "MuCCC Nix Cache";
  };

  config = lib.mkIf cfg.enable {
    services.nixos-passthru-cache = {
      enable = true;
      hostName = "cache.club.muc.ccc.de";
      cacheSize = "10G";
    };

    # sops.secrets.attic_credentials = { };
    #
    # systemd.services.atticd.serviceConfig.SupplementaryGroups = [ config.users.groups.keys.name ];
    #
    # networking.firewall.allowedTCPPorts = [ 8080 ];
    #
    # services.atticd = {
    #   enable = true;
    #   credentialsFile = config.sops.secrets.attic_credentials.path;
    #   settings = {
    #     listen = "[::]:8080";
    #     chunking = {
    #       nar-size-threshold = 64 * 1024;
    #       min-size = 16 * 1024;
    #       avg-size = 64 * 1024;
    #       max-size = 256 * 1024;
    #     };
    #   };
    # };
  };
}
