{ inputs, ... }:
{
  flake = {
    # regular colmena hive
    colmena = import ./hive.nix { inherit inputs; };
  };

  perSystem =
    { pkgs, ... }:
    {
      legacyPackages.colmenaNixosConfigurations =
        (import "${pkgs.colmena.src}/src/nix/hive/eval.nix" {
          rawFlake = inputs.self;
          colmenaOptions = import "${pkgs.colmena.src}/src/nix/hive/options.nix";
          colmenaModules = import "${pkgs.colmena.src}/src/nix/hive/modules.nix";
          hermetic = true;
        }).nodes;
    };
}
