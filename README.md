# muCCC NixOS Deployment

This repository is a Nix flake and provides a development shell with all required
tools. Use `nix develop` to drop into a shell or just use `direnv` with
`nix-direnv`.

## Tools used

 * [Nix Flakes](https://wiki.nixos.org/wiki/Flakes)
   * You need the [Nix package manager](https://nixos.org/) to use this repository
   * NixOS is not required but encouraged. Stay away from Darwin!
 * Deployment with [Colmena](https://github.com/zhaofengli/colmena)
 * Secrets Management with [sops-nix](https://github.com/Mic92/sops-nix)
 * Development Environment with `nix develop` or [direnv](https://direnv.net/)

## Common Tasks

### Deploy a host

```
colmena apply -v --on briafzentrum
colmena apply -v --build-on-target --on loungepi
```

### Update input flakes

```
nix flake update --commit-lock-file
```

### Check if all outputs evaluate

```
nix flake check
```

### Add a new host

Add host definition in `hive.nix` and configuration module in `hosts/hostname.nix`;

### Build images

```
nix build .#proxmoxImages.base
nix build .#proxmoxImages.nixbus
nix build .#sdImages.hauptraumpi
```

### Remote builds

[Remote building](https://nix.dev/manual/nix/2.26/advanced-topics/distributed-builds) on `bauwagen` can be done using the `bauwat` wrapper.

```
bauwat colmena apply --on ...
bauwat nix build ...
```

Add `bauwagen.env.club.muc.ccc.de` to your SSH configuration:
```
  Host bauwagen.env.club.muc.ccc.de
    User ...                          # your username, must be in group wheel (infra)
    IdentityFile ...                  # we require key authentication
    # ProxyJump transrapid.muc.ccc.de # v6-only external / non-VPN access
```

### Enter a Nix REPL to inspect configs

```
nix run .#repl luftschleuse
```

Or in development shell:

```
repl zock
```

### Apply a configuration locally without colmena (don't)

```
nixos-rebuild switch --flake .#auth
nixos-rebuild switch --flake .#loungepi --target-host root@loungepi.club.muc.ccc.de
```

### Reencrypt all sops secrets after key changes

```
nix run .#sops-updatekeys
```
