{ inputs, ... }:
{
  imports = [
    inputs.treefmt-nix.flakeModule
    inputs.pre-commit-hooks.flakeModule
  ];

  perSystem =
    { ... }:
    {
      treefmt = {
        projectRootFile = "flake.nix";
        programs = {
          deadnix.enable = true;
          nixfmt.enable = true;
        };
      };

      pre-commit = {
        settings = {
          hooks = {
            treefmt.enable = true;
            nil.enable = true;
          };
        };
      };
    };
}
