let
  users = {
    andi = {
      age = "age1xawzers83lsmtgj6pwl0uzdu949285ylctwpjhldedahm598desq67zz9d";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPYMT0xzYgUB+JVT5mj8/OBtt8lZxnC3WMVZRnYANT6E andi@mpb2"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDxGi9xnm0cXhNbiUjgq6K6vNyeoGr7f1NuGsvKmY6jK andi@mpb3"
      ];
    };
    bnut = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIel6b0dwaLpVpZdBFDT1XSYvHuHEQ8btX6JXHAirdrh bnut@NixOS-BNut"
      ];
    };
    butz = {
      age = "age1zk3ag6r4aje8yenjltu5x9tu4zj7pq8j4lhmgtt4dxq6aa5fgfqqhwax6z";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMWcXGzx3Eb2pNV22MCBcAIGqw9a5duxTour4u5Dk7d+ butz"
      ];
    };
    codec = {
      age = "age1cc6dvz6jv8rgg086stuwxdxw76h6e4fu98d8v73t7lh0wpt4q5zsxhpdy2";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAourBzt8am4sJc1v02elLTSVApWy79nlnt0dwoqdDCt codec"
      ];
    };
    derandere = {
      age = "age1val40xgjls29rhmf4a6gzhaa23t9w5mj6rrv5xtwqa3rxk4v4ptspya4vz";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN7IyhxIzMjHGq8dBi1jZDYfTBMRm7Z3l+U5ORNveh5e derandere"
      ];
    };
    derchris = {
      age = "age1w5lu3zmrs5wqh74447d8zt28dluhgtz6a4cyr6njzpjqyfgjz5usg7d6ms";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP3VMGqSKqxQYbCTc1QPwTc3m8eHPSMqwUgE4D1k9Z95 derchris@air.derchris.local"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOTClYJw4oZoJyVO8OUVbVeLA7nJPWwapEF5wNmguzG/ derchris@nixos"
      ];
    };
    fpletz = {
      age = "age1sj6kx043vqxcet6fv05mzjs9umpd0yu4xfln99yh8jpkt07w3e6sf9rzxh";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK20Lv3TggAXcctelNGBxjcQeMB4AqGZ1tDCzY19xBUV fpletz@lolnovo"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJY+/tAXZFm9U+nJt0kKo6e/TrYiH7E49n0ktbuF5I6 fpletz@fpine"
      ];
    };
    markus = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILUiwpETwMRVueebO8aC6fBv0uYvuByJPPnpczP8kAIP markus"
      ];
    };
    neunr = {
      age = "age1ec6zepklcpsnevn36try63v7fxmqhmh66g0epk3jlj7v66lw6q3sv7luem";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDyWcvk8smOkAtTBI0WDw+VmiGw4jOxvCt1LsCXJMrO+ 9R"
      ];
    };
    tuedel = {
      age = "age14wvkq35xqgpugvnz095l8k872qvywcz5jaqt4u4ptruv8k3rduasuu2ut7";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPZijVIKDZabaCQWBfQABUeGlDP4tc9Vz174R0vHqR6A tuedel@pandan"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILOJN+8b1OlGMJ0q+CYV4PbEA12ZEymZ/+HYws1ma0oM tuedel@kalonji"
      ];
    };
    linus = {
      age = "age1rfu5u02xcan2fqk38x62ddk7ayrmqcmxp50x5r336t0m0jdyugyqcf70f2";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN3EmXYSXsimS+vlGYtfTkOGuwvkXU0uHd2yYKLOxD2F linus@geruest"
      ];
    };
    sebtm = {
      age = "age1ugn0khxdjm8sfpqd2jll32rlhzc7xpjl3s4c9urrp449xw5y2y4sdnmmxa";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICk8HA/hrBTY/LeXV6gARww046iR9AAq83xk75VC3Z/0 SebTM@Personal"
      ];
    };
    q3k = {
      age = "age1q554587tphsrt6u5mhwesqupdfv69fthdf5enaeggdxqfw6llucszef5r2";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG599UildOrAq+LIOQjKqtGMwjgjIxozI1jtQQRKHtCP q3k@mimeomia"
      ];
    };
  };

  groups = {
    infra-admins = [
      "andi"
      "butz"
      "codec"
      "derandere"
      "derchris"
      "fpletz"
      "linus"
      "markus"
      "neunr"
      "tuedel"
      "q3k"
    ];

    hetzner = [
      "codec"
      "fpletz"
      "derchris"
    ];
  };
in
rec {
  getAgeForGroup = group: builtins.map (user: users."${user}".age or null) groups.${group};

  getUsersByGroup =
    group:
    builtins.listToAttrs (
      builtins.map (user: {
        name = user;
        value = users.${user};
      }) groups.${group}
    );

  mapUsersByGroup = group: f: builtins.mapAttrs f (getUsersByGroup group);

  authorizedKeysMixinForUser = user: { inherit (users.${user}) openssh; };

  getSSHKeysForUser = user: users.${user}.openssh.authorizedKeys.keys;

  getSSHKeysForUsers =
    userList: builtins.concatLists (builtins.map (user: getSSHKeysForUser user) userList);
}
