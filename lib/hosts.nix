let
  hosts = {
    transrapid.age = "age1zedul0usnk54dwxxf4vt9xhcq0qu9rpakkzm957un3actsh5l9ksxm64sg";
    luftschleuse.age = "age18u35epuh2tleuyfwvpank2pv57r4ql0hp6sefejdsxhssxumqgfss6h8r2";
    nixbus.age = "age1w2fvawmwamh99eplqxk8c83pyslgvqgfe82s3vgzuqsjn522xpjs2ndeyq";
    prometheus.age = "age1lqqalx673um7e6cf4grmqzk8k7pgetlmgq8nk09a74reny2xqdgscrylx8";
    briafzentrum.age = "age1p0c6xckd8uy433wj5z9grakze3jt459tv90amy6k2fv8zdr5zd9sp2l58l";
    auth.age = "age1wu4te6adjhtax4c97crl2juvth8wm5e623k5vxppdyguyrnkxppsvg6lau";
    nextcloud.age = "age14rktqzlxd8va4nh8q8074f64snwe7jd802tssnld8sx4zrgmg3rscpzysd";
    netbox.age = "age1p6u2pydnp50aef0nxnjd3ksx48j7azqs2jyarmk57lwtznqxf9rqtl4585";
    pad.age = "age14md2re7jwesmtf62af5ltjd2ew6cnez45ay5ezdrvj0jlvlj0g0syuf72f";
    forgejo.age = "age15t67yh2m8yuxpc4fkyrtxefffjqmr3dgq3wx4jrptgc6d2y8qg4q92d078";
    bauwagen.age = "age19vtkr00uvwwau75h6ht83wau5ksmw0ntjt66ddpvnxt94eyx34eswrk6yy";
    oaarchkatzl.age = "age1l6cvvn6ht0ecaz63qhwafmccshlp7c63n7txrmy960qw3llqnd6sc70nhk";
    stammstrecke.age = "age1lnhqzmv5wlw9npansqdca0zlun5njkncpdxm49c4d46qctek8qhs080fwv";
    nginx.age = "age1lfqyj6hdavvecm7tlqq5v8ywzfa4mchpq3clgy2402ptmp5mcvss0cgckr";
    hauptraumpi.age = "age1dsxtd7e038ltzt7xzp84fxk3xhrgxs35hhgnsl0uwyzuntwgev7s2jy5cg";
    loungepi.age = "age14qlcsjdschs870ghp557hvnkelzjhuwscp5tlgqrdq2w46g4tyxs6cu70j";
    drugga.age = "age13q0ark7eypym9p2khwmu8vw9m4p4h5fcxrg5tedqqrvgu9l82fhqgq7xj3";
  };
  hostNamesForSopsRules = {
    multimedia = [
      "loungepi"
      "hauptraumpi"
    ];
  };
in
{
  mapAllHosts = f: builtins.mapAttrs f hosts;
  mapAllHostsToList = f: builtins.map (host: f host hosts.${host}) (builtins.attrNames hosts);
  hostsForSopsRules =
    f:
    builtins.map (regex: f regex (map (host: hosts.${host}) hostNamesForSopsRules.${regex})) (
      builtins.attrNames hostNamesForSopsRules
    );
}
